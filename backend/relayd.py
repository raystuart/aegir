from celery import Celery
from celery.utils.log import get_task_logger
import json
import os
import re
import shlex
import subprocess

#TODO: split credentials into shared library
relayd = Celery('aegir', broker='pyamqp://aegir:53cr3t!@rabbitmq//')

relayd.conf.update(
    task_serializer='json',
    result_serializer='json',
    result_backend = 'redis://redis/0',
)

log = get_task_logger(__name__)

@relayd.task
def echo(text):
    ''' Dummy task to test connections to the queue. '''
    log.info('Received echo task: ' + text)
    return text

@relayd.task
def queue_valid(worker):
    ''' Run drupal aegir:debug to set queue_valid state TRUE. '''
    log.info('Received queue_valid task for '+ worker + '.')
    cmd = get_aegir_cmd('validate_queue', [worker])
    log.info('Running command: ' + cmd)
    run_cmd(cmd)

  # ARGS: --uri, --root, aegir_entity_type, entity_uuid, field_name, field_value
  # we need to be able to:
  #  * (over)write field contents,
  #  * append to field contents,
  #  * add a new value to multi-value fields
  #  (maybe this is a switch, or just implement different drupal console commands)
  #command: "/var/www/html/bin/drupal aegir:input --uri=aegir.ddev.site --root=/var/www/html aegir_platform {{ aegir_uuids.aegir_platform }} field_install_profiles '{{ profile_info_contents | to_json() }}'"
  #args:
  # @TODO This path should probably be set in a global variable.
  #  chdir: '/var/www/html/sites/aegir.ddev.site'
@relayd.task
def aegir_input(config, data):
    ''' Run drupal aegir:input with specified aegir entity, field and data. '''
    cmd = get_aegir_input_cmd(config, data)
    log.info('Running command: ' + cmd)
    run_cmd(cmd)

@relayd.task
def aegir_log(config, data):
    ''' Run drupal aegir:log with specified aegir_operation data. '''
    cmd = get_aegir_log_cmd(config, data)
    log.info('Running command: ' + cmd)
    run_cmd(cmd)

@relayd.task
def aegir_exitcode(config, exitcode):
    ''' Run drupal aegir:exticode with specified aegir_operation exitcode. '''
    cmd = get_aegir_exitcode_cmd(config, exitcode)
    log.info('Running command: ' + cmd)
    run_cmd(cmd)

# TODO: sanitize these arguments (shlex.quote() or equivalent)
def get_aegir_cmd(cmd, args=[]):
    cmd = '/var/www/html/bin/drupal aegir:' + cmd + ' '
    cmd += '--uri=aegir.ddev.site '
    cmd += '--root=/var/www/html '
    cmd += ' '.join(args)
    return cmd

def get_aegir_input_cmd(config, data):
    cmd = get_aegir_cmd('input')
    cmd += config['entity_type'] + ' '
    cmd += config['entity_uuid'] + ' '
    cmd += config['field_name'] + ' '
    cmd += '\"{}\"'.format(data)
    return cmd

def get_aegir_log_cmd(config, data):
    cmd = get_aegir_cmd('log')
    cmd += config['uuid'] + ' '
    cmd += str(data['log_timestamp']) + ' '
    cmd += '"' + re.escape(data['log_output']) + '" '
    cmd += str(data['log_sequence']) + ' '
    return cmd

def get_aegir_exitcode_cmd(config, exitcode):
    cmd = get_aegir_cmd('exitcode')
    cmd += config['uuid'] + ' '
    cmd += str(exitcode) + ' '
    return cmd

def run_cmd(cmd):
    ''' Run a system command and post the output to the frontend. '''
    cmd = shlex.split(cmd)
    env = os.environ.copy()
    env["PHP_MEMORY_LIMIT"] = "-1"
    seq = 0
    try:
        process = subprocess.Popen(cmd,
                             env=env,
                             cwd='/var/www/html',
                             stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT,
                             bufsize=1)
        for line in iter(process.stdout.readline, b''):
            log.info('Command output: '+line.decode())
        process.stdout.close()
        exitcode = process.wait()
    except RuntimeError as e:
        log.error('Running command "%s" failed: %s', cmd, e.strerror)
