# Install (and clean up) Drupal site.

.PHONY: install install-real ci-install uninstall uninstall-real

SITE_INSTALL_CMD = site:install $(INSTALL_PROFILE)\
                       --site-name=$(SITE_NAME) \
                       --yes --locale="en" \
                       --db-url="mysql://$(DB_USER):$(DB_PASS)@$(DB_HOST)/$(DB_NAME)" \
                       --sites-subdir=$(SITE_URL) \
                       --account-name="$(ADMIN_USER)" \
                       --account-mail="dev@$(SITE_URL)" \
                       --account-pass="$(ADMIN_PASS)" \
                       --debug -vvv

install: ##@aegir Ægir: Install Drupal site.
				@$(MAKE-QUIET) install-real
install-real:
				@$(ECHO) "$(YELLOW)Beginning installation of $(GREY)$(SITE_URL)$(YELLOW). (Be patient. This may take a while.)$(RESET)"
				$(DRUSH) $(SITE_INSTALL_CMD) $(QUIET)
				$(DRUSH) cache-rebuild $(QUIET)
				@$(ECHO) "$(YELLOW)Completed installation of $(GREY)$(SITE_URL).$(RESET)"

uninstall: ## Uninstall Drupal site.
				@$(MAKE-QUIET) uninstall-real
uninstall-real:
				-$(DRUSH) -y sql:drop $(QUIET)
				chmod 700 web/sites/$(SITE_URL)/
				rm -rf web/sites/$(SITE_URL)/files/config*
				rm -f web/sites/$(SITE_URL)/settings.php
				@$(ECHO) "$(YELLOW)Deleted $(GREY)$(SITE_URL).$(RESET)"

locale: ## Check and update Locale module for translation updates.
				$(DRUSH) locale-check && $(DRUSH) locale-update && $(DRUSH) cr
