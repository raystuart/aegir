@smoke-test @api @chromedriver
Feature: Chromedriver is available to run javascript.
  In order to test javascript functionality
  As an Developer
  I need to be able to run tests through chromedriver.

  Scenario: Confirm that login works without javascript.
    Given I am logged in as a "Administrator"

  @javascript
  Scenario: Confirm that login works with javascript (via chromedriver).
    Given I am logged in as a "Administrator"



