@wip
Feature: Developer tools
  In order to accelerate development of Ægir
  as a Developer,
  I want to be able to securely and easily enable and disable developer tools

  @security
  Scenario: Ensure developer tools are disabled by default
    Given I run "drush pm-list --status=enabled --type=module --format=list"
     Then I should not get:
     """
     aegir_devel
     devel
     features_ui
     page_manager_ui
     panels_ipe
     views_ui
     """

# TODO: write this test, logging into the front-end, etc.
#  @security @wip
#  Scenario: Ensure developer tools cannot be enabled from the front-end

  @devel
  Scenario: Enable developer tools
    Given I run "drush -y en aegir_devel"
      And I run "drush pm-list --status=enabled --type=module --format=list"
     Then I should get:
     """
     aegir_devel
     devel
     features_ui
     page_manager_ui
     panels_ipe
     views_ui
     """

  @devel
  Scenario: Disable developer tools
    Given I run "drush -y pm-uninstall aegir_devel"
      And I run "drush pm-list --status='not installed' --type=module --format=list"
     Then I should get:
     """
     aegir_devel
     features_ui
     views_ui
     devel
     """

