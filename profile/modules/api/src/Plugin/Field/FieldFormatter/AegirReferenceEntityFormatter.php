<?php

namespace Drupal\aegir_api\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;

/**
 * Plugin implementation of the 'entity reference rendered entity' formatter.
 *
 * @FieldFormatter(
 *   id = "aegir_reference_entity_view",
 *   label = @Translation("Rendered (task or operation) entity"),
 *   description = @Translation("Display the referenced (task or operation) entities rendered by entity_view()."),
 *   field_types = {
 *     "task_reference",
 *     "operation_reference"
 *   }
 * )
 */
class AegirReferenceEntityFormatter extends EntityReferenceEntityFormatter {


}
