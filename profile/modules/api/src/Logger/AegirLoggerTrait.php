<?php

namespace Drupal\aegir_api\Logger;

trait AegirLoggerTrait {

  /**
   * Return a new logger object.
   */
  protected function log() {
    return new AegirLogger();
  }

}
