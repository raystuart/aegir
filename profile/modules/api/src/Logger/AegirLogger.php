<?php

namespace Drupal\aegir_api\Logger;

use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;

class AegirLogger implements AegirLoggerInterface {

  use StringTranslationTrait, MessengerTrait, LoggerChannelTrait;

  // The message to log or display.
  protected $message;

  // The channel to which to log the message.
  protected $channel = 'aegir';

  // The severity of the message being displayed or logged.
  protected $severity = 'info';

  /**
   * {@inheritdoc}
   */
  public function info($message, $variables = []) {
    return $this->emit($message, $variables, 'info');
  }

  /**
   * {@inheritdoc}
   */
  public function success($message, $variables = []) {
    return $this->emit($message, $variables, 'success');
  }

  /**
   * Display and log a warning message.
   */
  public function warning($message, $variables = []) {
    return $this->emit($message, $variables, 'warning');
  }

  /**
   * {@inheritdoc}
   */
  public function error($message, $variables = []) {
    return $this->emit($message, $variables, 'error');
  }

  /**
   * {@inheritdoc}
   */
  public function emit($message, $variables = [], $severity = '') {
    return $this
      ->display($message, $variables, $severity)
      ->log($message, $variables, $severity)
      ->drush($message, $variables, $severity);
  }

  /**
   * {@inheritdoc}
   */
  public function display($message, $variables = [], $severity = '') {
    return $this
      ->setMessage($message, $variables)
      ->setSeverity($severity)
      ->displayMessage();
  }

  /**
   * {@inheritdoc}
   */
  public function log($message, $variables = [], $severity = '') {
    return $this
      ->setMessage($message, $variables)
      ->setSeverity($severity)
      ->logMessage();
  }

  /**
   * {@inheritdoc}
   */
  public function drush($message, $variables = [], $severity = '') {
    $this
      ->setMessage($message, $variables)
      ->setSeverity($severity)
      ->logToDrush();
    return $this;
  }

  /**
   * Register the translated message with any provided variables interpolated.
   */
  protected function setMessage(string $message, array $variables = []) {
    $this->message = $this->t($message, $variables);
    return $this;
  }

  /**
   * Set the severity of the message.
   */
  protected function setSeverity(string $severity) {
    $this->severity = $severity;
    return $this;
  }

  /**
   * Display the message at the appropriate level of severity.
   */
  protected function displayMessage(string $severity = '') {
    $severity = !empty($severity) ? $severity : $this->severity;
    switch ($severity) {
      case 'alert':
      case 'critical':
      case 'emergency':
      case 'error':
        $this->messenger()->addError($this->message);
        break;
      case 'debug':
        // Don't display debug messages to the screen.
        break;
      case 'warning':
        $this->messenger()->addWarning($this->message);
        break;
      case 'status':
      case 'success':
        $this->messenger()->addStatus($this->message);
      case 'info':
      case 'notice':
      default:
        $this->messenger()->addMessage($this->message);
    }
    return $this;
  }

  /**
   * Log the message at the appropriate level of severity.
   */
  protected function logMessage(string $severity = '') {
    $severity = !empty($severity) ? $severity : $this->severity;
    switch ($severity) {
      case 'alert':
        $this->getChannelLogger()->alert($this->message);
        break;
      case 'critical':
        $this->getChannelLogger()->critical($this->message);
        break;
      case 'debug':
        $this->getChannelLogger()->debug($this->message);
        break;
      case 'emergency':
        $this->getChannelLogger()->emergency($this->message);
        break;
      case 'error':
        $this->getChannelLogger()->error($this->message);
        break;
      case 'notice':
      case 'status':
      case 'success':
        $this->getChannelLogger()->notice($this->message);
        break;
      case 'warning':
        $this->getChannelLogger()->warning($this->message);
        break;
      case 'info':
      default:
        $this->getChannelLogger()->info($this->message);
    }
    return $this;
  }

  /**
   * Return a logger registered to the appropriate channel.
   */
  protected function getChannelLogger() {
    // @TODO set this to the calling class, if possible.
    return $this->getLogger($this->channel);
  }

  /**
   * Write to the Drush log.
   */
  protected function logToDrush() {
    if (!function_exists('drush_log')) return;
    drush_log($this->message, $this->severity);
    return $this;
  }

}
