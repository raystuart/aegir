<?php

namespace Drupal\aegir_api\Entity;

/**
 * Defines the default storage handler class for Ægir entities.
 *
 * This extends the base storage class, adding required special handling for
 * Ægir entities.
 *
 * @ingroup aegir_api
 */
class StorageBase extends AbstractStorage {

}
