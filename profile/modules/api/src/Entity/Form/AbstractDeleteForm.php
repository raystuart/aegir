<?php

namespace Drupal\aegir_api\Entity\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Ægir entities.
 *
 * @ingroup aegir_api
 */
abstract class AbstractDeleteForm extends ContentEntityDeleteForm {

}
