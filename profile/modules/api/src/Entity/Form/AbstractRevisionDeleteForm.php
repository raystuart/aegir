<?php

namespace Drupal\aegir_api\Entity\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\aegir_api\Logger\AegirLoggerTrait;

/**
 * Provides a form for deleting an Ægir entity revision.
 *
 * @ingroup aegir_api
 */
abstract class AbstractRevisionDeleteForm extends ConfirmFormBase {

  use AegirLoggerTrait, StringTranslationTrait;

  /**
   * The entity revision.
   *
   * @var \Drupal\aegir_api\Entity\EntityInterface
   */
  protected $revision;

  /**
   * The entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a new revision delete form.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $entity_storage
   *   The entity storage.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(EntityStorageInterface $entity_storage, Connection $connection) {
    $this->storage = $entity_storage;
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return $this->revision->getEntityTypeID() . '_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => format_date($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.' . $this->revision->getEntityTypeID() . '.version_history', [
      $this->revision->getEntityTypeID() => $this->revision->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $revision = NULL) {
    $this->revision = $this->storage->loadRevision($revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $revision = $this->revision;
    $entity_type = $revision->getEntityType();

    $this->storage->deleteRevision($revision->getRevisionId());
    $this->log()->success('Revision from %revision-date of %entity_label %title has been deleted.', [
      '%entity_label' => $entity_type->getSingularLabel(),
      '%revision-date' => format_date($revision->getRevisionCreationTime()),
      '%title' => $revision->label(),
    ]);

    $form_state->setRedirect(
      'entity.' . $entity_type->id() . '.canonical',
      [$entity_type->id() => $revision->id()]
    );
    $revision_count = $this->connection
      ->query('SELECT COUNT(DISTINCT :revision) FROM {' . $entity_type->id() . '_field_revision} WHERE id = :id', [
        ':revision' => $entity_type->getKey('revision'),
        ':id' => $revision->id(),
      ])->fetchField();
    if ($revision_count > 1) {
      $form_state->setRedirect(
        'entity.' . $entity_type->id() . '.version_history',
        [$entity_type->id() => $revision->id()]
      );
    }
  }

}
