<?php

namespace Drupal\aegir_api\Entity\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\aegir_api\Logger\AegirLoggerTrait;

/**
 * Form controller for Ægir entity edit forms.
 *
 * @ingroup aegir_api
 */
abstract class AbstractEntityForm extends ContentEntityForm {

  use AegirLoggerTrait;

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->log()->success('Created the %label @entity_type.', [
          '%label' => $entity->label(),
          '@entity_type' => $entity->getEntityType()->getSingularLabel(),
        ]);
        break;

      default:
        $this->log()->success('Saved the %label @entity_type.', [
          '%label' => $entity->label(),
          '@entity_type' => $entity->getEntityType()->getSingularLabel(),
        ]);
    }

    $form_state->setRedirect('entity.' . $entity->getEntityTypeId() . '.canonical', [
      $entity->getEntityTypeId() => $entity->id(),
    ]);
  }

}
