<?php

namespace Drupal\aegir_api\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for Ægir entities.
 *
 * @see Drupal\Core\Entity\Routing\AdminHtmlRouteProvider
 * @see Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 */
abstract class AbstractHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    $entity_type_id = $entity_type->id();

    if ($collection_route = $this->getCollectionRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.collection", $collection_route);
    }

    if ($history_route = $this->getHistoryRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.version_history", $history_route);
    }

    if ($revision_route = $this->getRevisionRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.revision", $revision_route);
    }

    if ($revert_route = $this->getRevisionRevertRoute($entity_type)) {
      $collection->add("{$entity_type_id}.revision_revert_confirm", $revert_route);
    }

    if ($delete_route = $this->getRevisionDeleteRoute($entity_type)) {
      $collection->add("{$entity_type_id}.revision_delete_confirm", $delete_route);
    }

    if ($translation_route = $this->getRevisionTranslationRevertRoute($entity_type)) {
      $collection->add("{$entity_type_id}.revision_revert_translation_confirm", $translation_route);
    }

    return $collection;
  }

  /**
   * Gets the collection route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getCollectionRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('collection') && $entity_type->hasListBuilderClass()) {
      $route = new Route($entity_type->getLinkTemplate('collection'));
      $route
        ->setDefaults([
          '_entity_list' => $entity_type->id(),
          '_title' => $entity_type->getLabel() . 's',
        ])
        ->setRequirement('_permission', "access aegir {$entity_type->getSingularLabel()} overview")
        ->setOption('_admin_route', TRUE);
    }
    return $route ?? NULL;
  }

  /**
   * Gets the version history route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getHistoryRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('version-history')) {
      $route = new Route($entity_type->getLinkTemplate('version-history'));
      $route
        ->setDefaults([
          '_title' => "{$entity_type->getLabel()} revisions",
          '_controller' => "\Drupal\\{$entity_type->getProvider()}\Entity\Controller::revisionOverview",
        ])
        ->setRequirement('_permission', "view all aegir {$entity_type->getSingularLabel()} revisions")
        ->setOption('_admin_route', TRUE);
    }
    return $route ?? NULL;
  }

  /**
   * Gets the revision route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getRevisionRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('revision')) {
      $route = new Route($entity_type->getLinkTemplate('revision'));
      $route
        ->setDefaults([
          '_controller' => "\Drupal\\{$entity_type->getProvider()}\Entity\Controller::revisionShow",
          '_title_callback' => "\Drupal\\{$entity_type->getProvider()}\Entity\Controller::revisionPageTitle",
        ])
        ->setRequirement('_permission', "view all aegir {$entity_type->getSingularLabel()} revisions")
        ->setOption('_admin_route', TRUE);
    }
    return $route ?? NULL;
  }

  /**
   * Return the module-specific revision revert form class.
   */
  function getRevisionRevertForm($entity_type) {
    return "\Drupal\\{$entity_type->getProvider()}\Entity\Form\RevisionRevertForm";
  }

  /**
   * Return the module-specific revision delete form class.
   */
  function getRevisionDeleteForm($entity_type) {
    return "\Drupal\\{$entity_type->getProvider()}\Entity\Form\RevisionDeleteForm";
  }

  /**
   * Return the module-specific revision translation revert form class.
   */
  function getRevisionRevertTranslationForm($entity_type) {
    return "\Drupal\\{$entity_type->getProvider()}\Entity\Form\RevisionRevertTranslationForm";
  }

  /**
   * Gets the revision revert route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getRevisionRevertRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('revision_revert')) {
      $route = new Route($entity_type->getLinkTemplate('revision_revert'));
      $route
        ->setDefaults([
          '_form' => $this->getRevisionRevertForm($entity_type),
          '_title' => 'Revert to earlier revision',
        ])
        ->setRequirement('_permission', "revert all aegir {$entity_type->getSingularLabel()} revisions")
        ->setOption('_admin_route', TRUE);
    }
    return $route ?? NULL;
  }

  /**
   * Gets the revision delete route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getRevisionDeleteRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('revision_delete')) {
      $route = new Route($entity_type->getLinkTemplate('revision_delete'));
      $route
        ->setDefaults([
          '_form' => $this->getRevisionDeleteForm($entity_type),
          '_title' => 'Delete earlier revision',
        ])
        ->setRequirement('_permission', "delete all aegir {$entity_type->getSingularLabel()} revisions")
        ->setOption('_admin_route', TRUE);
    }
    return $route ?? NULL;
  }

  /**
   * Gets the revision translation revert route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getRevisionTranslationRevertRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('translation_revert')) {
      $route = new Route($entity_type->getLinkTemplate('translation_revert'));
      $route
        ->setDefaults([
          '_form' => $this->getRevisionRevertTranslationForm($entity_type),
          '_title' => 'Revert to earlier revision of a translation',
        ])
        ->setRequirement('_permission', "revert all aegir {$entity_type->getSingularLabel()} revisions")
        ->setOption('_admin_route', TRUE);
    }
    return $route ?? NULL;
  }

}
