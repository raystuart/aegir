<?php

namespace Drupal\aegir_api\Entity;

/**
 * Creates entities automatically for certain reference fields.
 *
 * @ingroup aegir_api
 */
trait AutocreateTrait {

  /**
   * When creating the top-level parent entity the parent ID is not yet
   * available. This is because referenced entities are autocreated in
   * preSave(), so that their IDs can be registered in the reference fields. At
   * that point, the parent doesn't have an ID yet. In postSave(), when the ID
   * is available, the referenced entities are updated to include it.
   *
   * This affects, for example, how to construct an autocreated entity's name,
   * which should reference the top-level parent. Since we match existing
   * entities by name, and we want to re-use existing lower-level entities
   * (e.g. tasks), these names have to be consistent.
   *
   * @TODO Ensure entity names are unique.
   */

  /**
   * The types of reference fields to auto-create.
   *
   * @var array
   */
  protected $autocreateFieldTypes = ['task_reference', 'operation_reference'];

  /**
   * The current autocreate field.
   */
  protected $autocreateField = NULL;

  /**
   * The name of the top-level entity.
   */
  protected $autocreateParentName = NULL;

  /**
   * The ID of the top-level entity type.
   */
  protected $autocreateParentTypeId = NULL;

  /**
   * The name of the top-level entity type.
   */
  protected $autocreateParentTypeLabel = NULL;

  /**
   * Whether this is a top-level entity.
   */
  protected $creatingParentEntity = TRUE;

  /**
   * Create operations and tasks.
   */
  protected function createReferencedEntities() {
    foreach ($this->getAutocreateFields() as $name => $field) {
      $this->setAutocreateField($field);
      if ($this->autocreateFieldHasReferencedEntity()) continue;
      $entity = $this->createReferencedEntity();
      $this->setReferencedEntity($entity);
    }
  }

  /**
   * Delete operations and tasks.
   *
   * @TODO test coverage for this garbage collection mechanism (#21)
   */
  protected function deleteReferencedEntities() {
    foreach ($this->getAutocreateFields() as $name => $field) {
      $this->setAutocreateField($field);
      if (!$this->autocreateFieldHasReferencedEntity()) continue;
      $entity = $this->loadReferencedEntity();
      # This will trigger the preDelete() method on each referenced entity
      # recursively. The result is a depth-first deletion of all referenced
      # auto-created entities.
      $entity->delete();
    }
  }

  /**
   * Update operations and tasks.
   */
  protected function updateReferencedEntities() {
    foreach ($this->getAutocreateFields() as $name => $field) {
      $this->setAutocreateField($field);
      $this->updateReferencedEntity();
    }
  }

  /**
   * Set the current autocreate field.
   */
  protected function setAutocreateField($field) {
    $this->autocreateField = $field;
  }

  /**
   * Return the current autocreate field.
   */
  protected function getAutocreateField() {
    return $this->autocreateField;
  }

  /**
   * Update a referenced entity.
   */
  protected function updateReferencedEntity() {
    $entity = $this->loadReferencedEntity();

    // Sync names of referenced entities, in case of changes in parent.
    // Also inject the ID of the top-level entity, since it's available in
    // postSave(), even on initial entity creation.
    $this->setParentInfo($entity);
    $entity
      ->createNewRevision(FALSE)
      ->save();
  }

  /**
   * Load a referenced entity object.
   */
  protected function loadReferencedEntity() {
    $entities = $this
      ->getAutocreateField()['instance']
      ->referencedEntities();
    return reset($entities);
  }

  /**
   * Create an entity and return its ID.
   */
  protected function createReferencedEntity() {
    if ($this->matchingEntityExists()) {
      return $this->getMatchingEntity();
    }

    $entity = $this->createBaseReferencedEntity();

    $this->setParentInfo($entity);

    $entity->save();
    #if ($entity instanceof EntityOwnerInterface) {
    #  $entity->setOwnerId($uid);
    #}
    return $entity->id();
  }

  /**
   * Create an entity and return its ID.
   */
  protected function createBaseReferencedEntity() {
    $entity_type = $this->getAutocreateTargetEntityType();
    $bundle_key = $entity_type->getKey('bundle');
    $bundle = $this->getAutocreateTargetBundle();
    $label_key = $entity_type->getKey('label');
    $label = $this->getReferencedEntityName();

    $entity = \Drupal::entityTypeManager()
      ->getStorage($entity_type->id())
      ->create([
        $bundle_key => $bundle,
        $label_key => $label,
      ]);

    return $entity;
  }

  /**
   * Pass information about the parent to autocreated entities.
   */
  protected function setParentInfo($entity) {
    $entity->creatingParentEntity = FALSE;

    // Since we're in preSave(), no ID exists for $this yet.
    // So we'll populate referer_id in postSave().
    $entity->set('referer_id', $this->getParentId());
    $entity->set('referer_type', $this->getParentTypeId());

    $entity->autocreateParentId = $this->getParentId();
    $entity->autocreateParentName = $this->getParentName();
    $entity->autocreateParentTypeId = $this->getParentTypeId();
    $entity->autocreateParentTypeLabel = $this->getParentTypeLabel();
  }

  /**
   * Return the ID of the top-level entity.
   */
  protected function getParentId() {
    return $this->creatingParentEntity() ? $this->id() : $this->autocreateParentId;
  }

  /**
   * Return the name of the top-level entity.
   */
  protected function getParentName() {
    return $this->creatingParentEntity() ? $this->getName() : $this->autocreateParentName;
  }

  /**
   * Return the ID of the top-level entity type.
   */
  protected function getParentTypeId() {
    return $this->creatingParentEntity() ? $this->getEntityType()->id() : $this->autocreateParentTypeId;
  }

  /**
   * Return the name of the top-level entity type.
   */
  protected function getParentTypeLabel() {
    return $this->creatingParentEntity() ? $this->getEntityType()->getSingularLabel() : $this->autocreateParentTypeLabel;
  }

  /**
   * Determine whether we're processing the parent entity.
   */
  protected function creatingParentEntity() {
    return $this->creatingParentEntity;
  }

  /**
   * Determine whether an autocreate field entity already exists.
   */
  protected function matchingEntityExists() {
    return (bool) $this->getMatchingEntity();
  }

  /**
   * Return an existing entity for an autocreate field, if one exists.
   */
  protected function getMatchingEntity() {
    $entity_type = $this->getAutocreateTargetEntityTypeId();
    $query = \Drupal::service('entity_type.manager')
      ->getStorage($entity_type)
      ->getQuery()
      ->condition('type', $this->getAutocreateTargetBundle())
      ->condition('name', $this->getReferencedEntityName());
    $entity_ids = $query->execute();
    // @TODO Check for multiple IDs, and throw warning?
    return reset($entity_ids);
  }

  /**
   * Return the name of an entity referenced by an autocreate field.
   */
  protected function getReferencedEntityName() {
    $template = "':field_label' :target_entity_type (for ':parent_name' :parent_entity_type)";
    $tokens = [
      ':field_label' => $this->getAutocreateField()['definition']->label(),
      ':target_entity_type' => $this->getAutocreateTargetEntityType()->getSingularLabel(),
      ':parent_name' => $this->getParentName(),
      ':parent_entity_type' => $this->getParentTypeLabel(),
    ];
    return strtr($template, $tokens);
  }

  /**
   * Return the entity type definition for an autocreate field.
   */
  protected function getAutocreateTargetEntityType() {
    $id = $this->getAutocreateTargetEntityTypeId();
    return \Drupal::entityTypeManager()->getDefinition($id);
  }

  /**
   * Return the entity type ID for an autocreate field.
   */
  protected function getAutocreateTargetEntityTypeId() {
    $bundle = $this->getAutocreateTargetBundle();
    $bundles = \Drupal::service("entity_type.bundle.info")->getAllBundleInfo();
    foreach ($bundles as $entity_type => $labels) {
      if (array_key_exists($bundle, $labels)) {
        return $entity_type;
      }
    }
  }

  /**
   * Create and return a referenced entity.
   */
  protected function setReferencedEntity($entity_id) {
    $field_name = $this->getAutocreateField()['name'];
    $this->set($field_name, ['target_id' => $entity_id]);
  }

  /**
   * Given a field definition, return the bundle of the entity to autocreate.
   */
  protected function getAutocreateTargetBundle() {
    $field_config = $this->getAutocreateField()['definition'];
    return reset($field_config->getSetting('handler_settings')['target_bundles']);
  }

  /**
   * Determine whether an autocreate field already references and entity.
   */
  protected function autocreateFieldHasReferencedEntity() {
    $field = $this->getAutocreateField()['instance'];
    return !empty($field->referencedEntities());
  }

  /**
   * Get operation and task reference fields.
   */
  protected function getAutocreateFields() {
    $fields = [];
    foreach ($this->getBundleFieldDefinitions() as $name => $definition) {
      $type = $definition->getType();
      if (in_array($type, $this->getAutocreateFieldTypes())) {
        $fields[$name] = [
          'name' => $name,
          'instance' => $this->get($name),
          'definition' => $definition,
        ];
      }
    }
    return $fields;
  }

  /**
   * Return the field types to autocreate.
   */
  protected function getAutocreateFieldTypes() {
    // @TODO Move to hook to gather (and alter) from modules?
    return $this->autocreateFieldTypes;
  }

  /**
   * Determine whether the object has a "parent" entity.
   */
  public function hasReferencingEntity() {
    return !$this->get('referer_type')->isEmpty();
  }

  // @TODO Fix naming in this trait.
  // 'referencing entity' == 'parent' == 'referer'
  // 'referenced entity' ~~ 'autocreate field'

  /**
   * Return the top-level parent entity.
   */
  public function getReferencingEntity() {
    return \Drupal::entityTypeManager()
      ->getStorage($this->getRefererType())
      ->load($this->getRefererId());
  }

  /**
   * Return the top-level parent entity type.
   */
  protected function getRefererType() {
    return $this->get('referer_type')->first()->getString();
  }

  /**
   * Return the top-level parent entity ID.
   */
  protected function getRefererId() {
    return $this->get('referer_id')->first()->getString();
  }

}
