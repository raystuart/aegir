<?php

namespace Drupal\aegir_api\Entity\EntityType\Form;

/**
 * Default form to delete Ægir entity types.
 */
class DeleteFormBase extends AbstractDeleteForm {

}
