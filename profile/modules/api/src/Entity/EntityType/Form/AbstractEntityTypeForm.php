<?php

namespace Drupal\aegir_api\Entity\EntityType\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\aegir_api\Logger\AegirLoggerTrait;

/**
 * Class AbstractEntityTypeForm.
 *
 * @package Drupal\aegir_api\Form
 */
abstract class AbstractEntityTypeForm extends EntityForm {

  use AegirLoggerTrait;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t("Label for the %entity_type.", [
        '%entity_type' => $this->entity->getEntityType()->getLabel(),
      ]),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\\' . $this->entity->getEntityType()->getBundleOf() . '\Entity\EntityType::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = $this->entity->save();

    switch ($status) {
      case SAVED_NEW:
        $this->log()->success('Created the %label @entity_type.', [
          '%label' => $this->entity->label(),
          '@entity_type' => $this->entity->getEntityType()->getSingularLabel(),
        ]);
        break;

      default:
        $this->log()->success('Saved the %label @entity_type.', [
          '%label' => $this->entity->label(),
          '@entity_type' => $this->entity->getEntityType()->getSingularLabel(),
        ]);
    }
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
  }

}
