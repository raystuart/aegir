<?php

namespace Drupal\aegir_api\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Ægir entities.
 *
 * @ingroup aegir_api
 */
abstract class AbstractListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.' . $entity->getEntityTypeId() . '.canonical',
      [ $entity->getEntityTypeId() => $entity->id() ]
    );
    return $row + parent::buildRow($entity);
  }

}
