<?php

namespace Drupal\aegir_api\Entity;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for Ægir entities.
 *
 * @see \Drupal\aegir_api\Entity\AbstractEntity.
 */
abstract class AbstractAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    $entity_type = (string) $entity->getEntityType()->getSingularLabel();
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, "view unpublished aegir {$entity_type} entities");
        }
        return AccessResult::allowedIfHasPermission($account, "view published aegir {$entity_type} entities");

      case 'update':
        return AccessResult::allowedIfHasPermission($account, "edit aegir {$entity_type} entities");

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, "delete aegir {$entity_type} entities");
    }

    // Unknown operation, no opinion.
    // @codeCoverageIgnoreStart
    return AccessResult::neutral();
    // @codeCoverageIgnoreEnd
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    $entity_type = (string) \Drupal::entityTypeManager()
      ->getStorage($context['entity_type_id'])
      ->getEntityType()
      ->getSingularLabel();
    return AccessResult::allowedIfHasPermission($account, "add aegir {$entity_type} entities");
  }

}
