<?php

namespace Drupal\aegir_api\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\aegir_api\Entity\AutocreateTrait;
use Drupal\user\UserInterface;

/**
 * Defines an Ægir entity.
 *
 * @ingroup aegir_api
 */
abstract class AbstractEntity extends RevisionableContentEntityBase implements EntityInterface {

  use AutocreateTrait;
  use EntityChangedTrait;
  use MarshalTrait;
  use StringTranslationTrait;

  /**
   * Whether to create a new revision.
   *
   * @var bool
   */
  protected $createNewRevision = TRUE;

  /**
   * Check whether to create a new revision.
   */
  public function shouldCreateNewRevision() {
    return $this->createNewRevision;
  }

  /**
   * Set whether to create a new revision.
   */
  public function createNewRevision($value = TRUE) {
    $this->createNewRevision = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    $this->setDefaultTranslationOwners()
      ->setDefaultRevisionAuthor();
    $this->createReferencedEntities();
  }

  /**
   * {@inheritdoc}
   * @TODO Should this be implemented in delete() or postDelete() instead?
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    parent::preDelete($storage, $entities);
    foreach ($entities as $entity) {
      $entity->deleteReferencedEntities();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    if ($this->shouldCreateNewRevision()) {
      $this->setNewRevision();
    }

    return parent::save();
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    // Upon initially saving this entity, any child entities created via
    // AutocreateTrait won't have a reference back to this one yet.
    if (!$update) {
      // @TODO We also want to update referenced entities when the parent's
      // name changes.
      $this->updateReferencedEntities();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setNewRevision($value = TRUE) {
    parent::setNewRevision($value);
    $this->setRevisionCreationTime(REQUEST_TIME);
    // Save the current user as revision author.
    $this->setRevisionUserId(\Drupal::currentUser()->id());
  }

  /**
   * Set default owner for each translation.
   */
  protected function setDefaultTranslationOwners() {
    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }
    return $this;
  }

  /**
   * Set default author for current revision.
   */
  protected function setDefaultRevisionAuthor() {
    // If no revision author has been set explicitly, make the
    // entity owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->bundle();
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 128,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the entity is published.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    $fields['revision_log_message']
      ->setDisplayOptions('form', ['region' => 'hidden'])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['operation_status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Operation status'))
      ->setDescription(t('The status of the operation.'))
      ->setRevisionable(TRUE)
      ->setReadOnly(TRUE)
      ->setDefaultValue('none')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['referer_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Referenced by'))
      ->setDescription(t('The ID of the entity that references this one.'));

    $fields['referer_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Referenced by type'))
      ->setDescription(t('The type of entity that references this one.'));

    return $fields;
  }

}
