<?php

namespace Drupal\aegir_api\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Annotations\DrupalCommand;

/**
 * Class DeleteCommand.
 *
 * @package Drupal\aegir_api
 *
 * @DrupalCommand (
 *   extension="aegir_api",
 *   extensionType="module"
 * )
 * @TODO Add tests.
 */
class DeleteCommand extends AbstractAegirCommand {

  protected $command_name = 'delete';

  protected $confirm = TRUE;

  protected $options = ['entity_type', 'id', 'name'];
  protected $entity_type = FALSE;
  protected $id = FALSE;
  protected $name = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    if (!$this->confirmed()) return;
    if ($this->name) {
      $this->id = $this->getIdFromName();
    }
    if ($this->deleteEntity()) {
      $this->io()->success($this->translate('success'));
    }
    else {
      $this->io()->error($this->translate('failure'));
    }
  }

  protected function promptForOption($option) {
    switch ($option) {
      case 'entity_type':
        $choices = $this->getAegirEntityTypeLabels();
        return $this->chooseOption($option, $choices);
      case 'id':
        $choices = $this->getAegirEntityChoices($this->entity_type);
        return $this->chooseOption($option, $choices);
      default:
        # TODO Throw an appropriate exception.
    }
  }

  protected function getIdFromName() {
    $ids = \Drupal::entityQuery($this->entity_type)
      ->condition('name', $this->name, '=')
      ->execute();
    return reset($ids);
  }

  protected function getReplacement($option) {
    switch ($option) {
      case 'entity_type':
        return $this->getEntityTypeLabel();
      case 'id':
        return $this->id;
      case 'name':
        return $this->name;
      default:
        # TODO Throw an appropriate exception.
    }
  }

  protected function getEntityTypeLabel() {
    return $this->getAegirEntityTypeLabels()[$this->entity_type];
  }

  /**
   * Delete an entity.
   */
  protected function deleteEntity() {
    $entity = $this->loadEntity();
    if (!$entity) return FALSE;
    $entity->delete();
    return TRUE;
  }

  /**
   * Load an entity.
   */
  protected function loadEntity() {
    $entity = $this
      ->entityTypeManager
      ->getStorage($this->entity_type)
      ->load($this->id);
    if (is_null($entity)) {
      return $this->io()->error(
        $this->translate('error.no-entity')
      );
    }
    return $entity;
  }

}
