<?php

namespace Drupal\aegir_api\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Core\Command\Command;

/**
 * Class AbstractAegirCommand.
 *
 * @package Drupal\aegir_api
 */
abstract class AbstractAegirCommand extends Command {

  use OptionHandlingTrait;
  use EntityApiTrait;

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName($this->getCommand())
      ->setDescription($this->trans($this->getCommandKey() . 'description'))
      ->configureOptions();
  }

  /**
   * {@inheritdoc}
   */
  protected function initialize(InputInterface $input, OutputInterface $output) {
    parent::initialize($input, $output);
    $this->registerOptions();
  }

  /**
   * {@inheritdoc}
   */
  protected function interact(InputInterface $input, OutputInterface $output) {
    $this->determineOptions();
  }

}
