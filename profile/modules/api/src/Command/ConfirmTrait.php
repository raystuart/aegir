<?php

namespace Drupal\aegir_api\Command;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\ConfirmationQuestion;

/**
 * Trait ConfirmTrait.
 *
 * @package Drupal\aegir_api
 */
trait ConfirmTrait {

  protected $confirm = FALSE;

  protected $confirmed = FALSE;

  protected function askConfirmation() {
    $question = new ConfirmationQuestion(
      $this->translate('confirm') . ' ',
      FALSE
    );
    $this->confirmed = $this->getHelper('question')
      ->ask($this->input(), $this->io(), $question);
  }

  protected function confirmed() {
    return $this->confirmed;
  }

  protected function registerConfirmation() {
    $this->confirmed = $this->input()->getOption('yes');
  }

  protected function determineConfirmation() {
    $this->confirmed || $this->askConfirmation();
  }

}
