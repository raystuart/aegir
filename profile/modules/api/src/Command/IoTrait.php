<?php

namespace Drupal\aegir_api\Command;

/**
 * Trait IoTrait.
 *
 * @package Drupal\aegir_api
 */
trait IoTrait {

  /**
   * Alias for getInput().
   */
  protected function input() {
    return $this->io()->getInput();
  }

  /**
   * Alias for getIO().
   */
  protected function io() {
    return $this->getIo();
  }

}
