<?php

namespace Drupal\aegir_api\Command;

/**
 * Trait CommandNameTrait.
 *
 * @package Drupal\aegir_api
 */
trait CommandNameTrait {

  protected $command_package = 'aegir';

  protected $command_name = 'TBD';

  protected function getCommandKey() {
    return "commands.{$this->command_package}.{$this->command_name}.";
  }

  protected function getCommand() {
    return "{$this->command_package}:{$this->command_name}";
  }

}
