<?php

namespace Drupal\aegir_site\Entity\Form;

use Drupal\aegir_api\Entity\Form\AbstractRevisionRevertForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for reverting a Ægir Site revision.
 *
 * @ingroup aegir_site
 */
class RevisionRevertForm extends AbstractRevisionRevertForm {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.field_manager')->getStorage('aegir_site'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aegir_site_revision_revert_confirm';
  }

}
