<?php

namespace Drupal\aegir_site\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\aegir_api\Entity\AbstractEntity;

/**
 * Defines an Ægir site entity.
 *
 * @ingroup aegir_site
 *
 * @ContentEntityType(
 *   id = "aegir_site",
 *   label = @Translation("Site"),
 *   bundle_label = @Translation("Site type"),
 *   handlers = {
 *     "storage" = "Drupal\aegir_api\Entity\StorageBase",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\aegir_api\Entity\ListBuilderBase",
 *     "views_data" = "Drupal\aegir_api\Entity\ViewsDataBase",
 *     "translation" = "Drupal\aegir_api\Entity\TranslationHandlerBase",
 *
 *     "form" = {
 *       "default" = "Drupal\aegir_api\Entity\Form\EntityFormBase",
 *       "add" = "Drupal\aegir_api\Entity\Form\EntityFormBase",
 *       "edit" = "Drupal\aegir_api\Entity\Form\EntityFormBase",
 *       "delete" = "Drupal\aegir_api\Entity\Form\DeleteFormBase",
 *     },
 *     "access" = "Drupal\aegir_api\Entity\AccessControlHandlerBase",
 *     "route_provider" = {
 *       "html" = "Drupal\aegir_api\Entity\HtmlRouteProviderBase",
 *     },
 *   },
 *   base_table = "aegir_site",
 *   data_table = "aegir_site_field_data",
 *   revision_table = "aegir_site_revision",
 *   revision_data_table = "aegir_site_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer aegir site entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   links = {
 *     "canonical" = "/admin/aegir/sites/{aegir_site}",
 *     "add-page" = "/admin/aegir/sites/add",
 *     "add-form" = "/admin/aegir/site/add/{aegir_site_type}",
 *     "edit-form" = "/admin/aegir/sites/{aegir_site}/edit",
 *     "delete-form" = "/admin/aegir/sites/{aegir_site}/delete",
 *     "version-history" = "/admin/aegir/sites/{aegir_site}/revisions",
 *     "revision" = "/admin/aegir/sites/{aegir_site}/revisions/{entity_revision}/view",
 *     "revision_revert" = "/admin/aegir/sites/{aegir_site}/revisions/{entity_revision}/revert",
 *     "translation_revert" = "/admin/aegir/sites/{aegir_site}/revisions/{entity_revision}/revert/{langcode}",
 *     "revision_delete" = "/admin/aegir/sites/{aegir_site}/revisions/{entity_revision}/delete",
 *     "collection" = "/admin/aegir/sites",
 *   },
 *   bundle_entity_type = "aegir_site_type",
 *   field_ui_base_route = "entity.aegir_site_type.canonical"
 * )
 */
class Entity extends AbstractEntity {

  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name']
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -50,
        'region' => 'content',
      ]);

    return $fields;
  }
}
