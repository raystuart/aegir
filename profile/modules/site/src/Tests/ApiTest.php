<?php

namespace Drupal\aegir_site\Tests;

use Drupal\simpletest\WebTestBase;
use Drupal\aegir_site\Entity\Entity as Site;

/**
 * Test to exercise API functions not covered by other tests.
 *
 * @group aegir
 */
class ApiTest extends WebTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'aegir_test_site',
  ];

  public $profile = 'aegir';

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * Disable schema validation for the base theme.
   *
   * @see: https://www.drupal.org/node/2860072
   */
  protected function getConfigSchemaExclusions() {
    return array_merge(parent::getConfigSchemaExclusions(), ['bootstrap.settings']);
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->user = $this->drupalCreateUser([], 'TestUser');
    $this->user->addRole('aegir_site_manager');
    $this->user->save();
    $this->drupalLogin($this->user);
  }

  /**
   * Tests that the home page loads with a 200 response.
   */
  public function testApiMethods() {
    $settings = [
      // Use one of the bundles exported to the 'aegir_test_site' module.
      'type' => 'test_site_type_1',
      'name' => 'TEST_SITE_A',
      'created' => time(),
      'user_id' => $this->rootUser->id(),
      'status' => 1,
    ];
    $site = Site::create($settings);

    $type = $site->getType();
    $this->assertEqual($type, $settings['type'], 'Get type (bundle) of Ægir site.');

    $name = $site->getName();
    $this->assertEqual($name, $settings['name'], 'Get name of Ægir site.');

    $new_name = 'TEST_SITE_B';
    $site->setName($new_name);
    $name = $site->getName();
    $this->assertEqual($name, $new_name, 'Set name of Ægir site.');

    $created = $site->getCreatedTime();
    $this->assertEqual($created, $settings['created'], 'Get creation time of Ægir site.');

    $new_created = time() - 10;
    $site->setCreatedTime($new_created);
    $created = $site->getCreatedTime();
    $this->assertEqual($created, $new_created, 'Set name of Ægir site.');

    $owner_id = $site->getOwnerId();
    $this->assertEqual($owner_id, $settings['user_id'], 'Get owner of Ægir site.');

    $new_user = $this->drupalCreateUser([], 'TestUser2');
    $site->setOwner($new_user);
    $owner_id = $site->getOwnerId();
    $this->assertEqual($owner_id, $new_user->id(), 'Set owner of Ægir site.');

    $published = $site->isPublished();
    $this->assertEqual($published, $settings['status'], 'Get published status of Ægir site.');

    $unpublished = FALSE;
    $site->setPublished($unpublished);
    $published = $site->isPublished();
    $this->assertEqual($published, $unpublished, 'Set published status of Ægir site.');

    $site->save();
    $this->drupalGet('admin/aegir/sites/' . $site->id());
    $this->assertResponse(200);
    $this->assertText($new_name, 'User with "Site manager" role can access un-published Ægir sites.');

  }

}
