@api @command @aegir:input
Feature: 'aegir:input' Drupal Console command allows setting a value.
  In order to get data into the front-end
  As a queue worker
  I need a console command that can alter entity fields.

  Background:
    Given I am logged in as an "Administrator"
      And "input_command_test" content:
    | nid | title             | uuid                                 | field_input_command_test_field | field_input_command_test_multi |
    | 100 | Input test node   | 1b1314f1-7bc9-48af-9012-8dededd22dee | Input Command test field data  |                                |
    | 101 | Input multi value | 1b1314f1-7bc9-48af-9012-8dededd22dea |                                | First value                    |

  Scenario: 'aegir:input' command changes field data.
     When I am on "/node/100"
     Then I should get a "200" HTTP response
      And I should see "Input test node"
      And I should see "Input Command test field data"
     # @TODO: Validate arguments/options
     When I run "drupal aegir:input --uri=aegir.ddev.site --root=/var/www/html node 1b1314f1-7bc9-48af-9012-8dededd22dee field_input_command_test_field 'Changed data'"
      And I am on "/node/100"
     Then I should see "Changed data"

  Scenario: 'aegir:input' command adds data to a multi-value field.
     When I am on "/node/101"
     Then I should get a "200" HTTP response
      And I should see "Input multi value"
      And I should see "First value"
     # @TODO: Consider how to handle limited multi-value fields.
     When I run "drupal aegir:input --uri=aegir.ddev.site --root=/var/www/html node 1b1314f1-7bc9-48af-9012-8dededd22dea field_input_command_test_multi 'Second value'"
      And I am on "/node/101"
     Then I should see "First value"
     Then I should see "Second value"
