@example @example-update-operation @api @wip @disabled
Feature: Example operation to update debug data.
  In order to alter data on sn example site
  as an Aegir administrator,
  I need to be able to enter new data into an update operation.

  Background:
    #@TODO: fix this to use proper role/permissions
    #Given I am logged in as an "Ægir administrator"
    Given I am logged in as an "administrator"
      And I am on "admin/aegir/sites"

  Scenario: Create new Example site instance.
    Given I click "Add Ægir site"
     Then I should be on "admin/aegir/sites/add"
      And I should see "Add site"
      And I should see the link "Example site with update"
     When I click "Example site with update" in the "content" region
     # @TODO: adjust this path to be consistent (site vs sites)
     Then I should be on "admin/aegir/site/add/example_site_update"
      And I should see "Name"
      And I should see "Add Example site with update"
      And I should see "The name of the entity"
      And I should see "Debug data"
     When I enter "Example site update" for "Name"
      And I enter "Example content (before)" for "Debug data"
      And I press "Save"
     Then I should see "Created the Example site update site."
      And I should see "Log debug data"
      And I should see "Update debug data"
     When I click "Run" in the "Update debug data" row
      # Then the operation should run
      # The run button is disabled
      # The gears turn
      # We ought to be able to click View and see the log scroll by
      And I wait "5" seconds
      #The "Log Debug Data Operation" row status should be "done"
     When I click "View"
      #And I wait for AJAX to finish
      And I wait "5" seconds
     Then I should see "Operation log" in the "drupal_modal" region
      And I should see "TASK [example_print_debug_data : Display all variables for the role.]"
      And I should see "\"field_example_debug_data\": \"Example content\""
