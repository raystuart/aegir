@example @example-site @api @wip @disabled @javascript
Feature: Example site setup
  In order to instantiate a new example site
  as an Aegir administrator,
  I need to be able to create and deploy a Example site.

  Background:
    #@TODO: fix this to use proper role/permissions
    #Given I am logged in as an "Ægir administrator"
    Given I am logged in as an "administrator"
      And I am on "admin/aegir/sites"

  Scenario: Create new Example site instance.
    Given I click "Add Ægir site"
     Then I should be on "admin/aegir/sites/add"
      And I should see "Add site"
      And I should see the link "Example site" in the "content" region
     When I click "Example site" in the "content" region
     # @TODO: adjust this path to be consistent (site vs sites)
     Then I should be on "admin/aegir/site/add/example_site"
      And I should see "Name"
      And I should see "Add Example site"
      And I should see "The name of the entity"
      And I should see "Debug data"
     When I enter "Example site" for "Name"
      And I enter "Example content" for "Debug data"
      And I press "Save"
     Then I should see "Created the Example site site."
      And I should see "Log debug data"
     When I run the "Log debug data" operation
      And I wait "10" seconds
      And I am on "/admin/aegir/operations/test-log"
     Then I should see "STATIC LOG DATA"
      And I should see "Log data: Example content"
      
      # @TODO: fix this in #58 to test javascript/AJAX behaviour properly.
      #  When I click "View"
     #And I wait for AJAX to finish
     # And I wait "5" seconds
      #And I am on the "" iframe
      #Then I should see "Operation log" in the "drupal_modal" region
      #And I should see "TASK [example_print_debug_data : Display all variables for the role.]"
      #And I should see "\"field_example_debug_data\": \"Example content\""
      #And I should see "STATIC LOG DATA"
      #And I should see "LOG DATA: Example content"
