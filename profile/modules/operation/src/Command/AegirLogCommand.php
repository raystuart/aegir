<?php

namespace Drupal\aegir_operation\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Drupal\Console\Annotations\DrupalCommand;
use Drupal\Console\Core\Command\ContainerAwareCommand;
use Drupal\Console\Core\Style\DrupalStyle;
use Drupal\Core\Entity\EntityRepository;

/**
 * Class AegirLogCommand.
 *
 * @package Drupal\aegir_log_command
 *
 * DrupalCommand (
 *   extension="aegir_log_command",
 *   extensionType="module"
 * )
 */
class AegirLogCommand extends ContainerAwareCommand {

  # @TODO: Fix translations. See:https://github.com/hechoendrupal/drupal-console/issues/3010

  // The entity type for Aegir operations. We should only be logging to operations.
  const OPERATION_ENTITY_TYPE = 'aegir_operation';

  /**
   * The EntityRepository service.
   *
   * @var EntityRepository
   */
  protected $entityRepository;

  /**
   * Constructs a new AegirInputCommand object.
   */
  public function __construct(EntityRepository $entity_repository) {
    $this->entityRepository = $entity_repository;
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('aegir:log')
      #->setDescription($this->trans('commands.aegir.log.description'))
      ->setDescription('Log data into an Aegir Operation.')
      ->setHidden(true)
      ->addArgument(
        'uuid',
        InputArgument::REQUIRED,
        #$this->trans('commands.aegir.log.arguments.uuid')
        $this->trans('The UUID of the entity into which we will be inputting data.')
      )
      ->addArgument(
        'timestamp',
        InputArgument::REQUIRED,
        #$this->trans('commands.aegir.log.arguments.timestamp')
        $this->trans('The unix timestamp of the log line.')
      )
      ->addArgument(
        'data',
        InputArgument::REQUIRED,
        #$this->trans('commands.aegir.log.arguments.data')
        $this->trans('The log data.')
      )
      ->addArgument(
        'sequence',
        InputArgument::REQUIRED,
        #$this->trans('commands.aegir.log.arguments.sequence')
        $this->trans('The sequence number (deprecated).')
      );
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $io = new DrupalStyle($input, $output);
    $args = $input->getArguments();
    foreach ($args as $key => $value) {
      $io->info($key . ': ' . $value);
    }

    $operation = $this->entityRepository->loadEntityByUuid(self::OPERATION_ENTITY_TYPE, $args['uuid']);

    if (is_null($operation)) {
      return $io->error(sprintf(
      #$this->trans('commands.aegir.log.errors.invalid-uuid'),
        $this->trans('No operation found with UUID %s'),
        $args['uuid']
      ));
    }

    $operation->task_log_output[] = stripslashes($args['data']);
    $operation->task_log_sequence[] = $args['sequence'];
    $operation->task_log_timestamp[] = $args['timestamp'];
    // Don't create a new revision for each line of the log output.
    $operation->createNewRevision(FALSE);
    $operation->save();

  }

}
