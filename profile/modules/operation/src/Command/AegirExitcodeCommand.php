<?php

namespace Drupal\aegir_operation\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Drupal\Console\Annotations\DrupalCommand;
use Drupal\Console\Core\Command\ContainerAwareCommand;
use Drupal\Console\Core\Style\DrupalStyle;
use Drupal\Core\Entity\EntityRepository;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class AegirExitcodeCommand.
 *
 * @package Drupal\aegir_exitcode_command
 *
 * DrupalCommand (
 *   extension="aegir_exitcode_command",
 *   extensionType="module"
 * )
 */
class AegirExitcodeCommand extends ContainerAwareCommand {

  use StringTranslationTrait;

  # @TODO: Fix translations. See:https://github.com/hechoendrupal/drupal-console/issues/3010

  // The entity type for Aegir operations. We should only be registering exit codes to operations.
  const OPERATION_ENTITY_TYPE = 'aegir_operation';

  /**
   * The EntityRepository service.
   *
   * @var EntityRepository
   */
  protected $entityRepository;

  /**
   * Constructs a new AegirExitcodeCommand object.
   */
  public function __construct(EntityRepository $entity_repository) {
    $this->entityRepository = $entity_repository;
    $this->log = \Drupal::logger('aegir_task_log');
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('aegir:exitcode')
      #->setDescription($this->trans('commands.aegir.exitcode.description'))
      ->setDescription('Register exit code for an Aegir Operation.')
      ->setHidden(true)
      ->addArgument(
        'uuid',
        InputArgument::REQUIRED,
        #$this->trans('commands.aegir.exitcode.arguments.uuid')
        $this->trans('The UUID of the entity into which we will be inputting data.')
      )
      ->addArgument(
        'exitcode',
        InputArgument::REQUIRED,
        #$this->trans('commands.aegir.exitcode.arguments.exitcode')
        $this->trans('The exit code of the operation.')
      );
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $io = new DrupalStyle($input, $output);
    $args = $input->getArguments();
    foreach ($args as $key => $value) {
      $io->info($key . ': ' . $value);
    }

    $operation = $this->entityRepository->loadEntityByUuid(self::OPERATION_ENTITY_TYPE, $args['uuid']);

    if (is_null($operation)) {
      return $io->error(sprintf(
      #$this->trans('commands.aegir.exitcode.errors.invalid-uuid'),
        $this->trans('No operation found with UUID %s'),
        $args['uuid']
      ));
    }

    $this->log->notice($this->t('Updating status of :label operation.', [':label' => $operation->label()]));
    $operation->task_exitcode = $args['exitcode'];
    // Don't create a new revision when updating the task exit code.
    $operation->createNewRevision(FALSE);
    $operation->updateStatus();
    $operation->save();

    // In our tests, we create stand-alone operations, that do not have parent
    // entities. So we bail here, in that case. This doesn't make sense in
    // normal operations, where we will want to update the status of the
    // parent.
    if (!$operation->hasReferencingEntity()) {
      return;
    }

    $referer = $operation->getReferencingEntity();
    $this->log->notice($this->t('Updating status of :label :type.', [
      ':label' => $referer->label(),
      ':type' => $referer->getEntityType()->getSingularLabel(),
    ]));
    $referer->operation_status = $operation->operation_status;
    // @TODO Figure out whether to create a new revision of the refering entity.
    #$referer->createNewRevision(FALSE);
    $referer->save();

  }

}
