<?php

namespace Drupal\aegir_operation\Entity;

use Drupal\aegir_api\Entity\AbstractEntityType;

/**
 * Defines an Ægir operation type entity.
 *
 * @ConfigEntityType(
 *   id = "aegir_operation_type",
 *   label = @Translation("Operation type"),
 *   handlers = {
 *     "list_builder" = "Drupal\aegir_api\Entity\EntityType\ListBuilderBase",
 *     "form" = {
 *       "add" = "Drupal\aegir_operation\Entity\EntityType\Form\EntityTypeForm",
 *       "edit" = "Drupal\aegir_operation\Entity\EntityType\Form\EntityTypeForm",
 *       "delete" = "Drupal\aegir_api\Entity\EntityType\Form\DeleteFormBase"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\aegir_api\Entity\EntityType\HtmlRouteProviderBase",
 *     },
 *   },
 *   config_prefix = "aegir_operation_type",
 *   admin_permission = "administer aegir operation types",
 *   bundle_of = "aegir_operation",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/aegir/operations/types/{aegir_operation_type}",
 *     "add-form" = "/admin/aegir/operations/types/add",
 *     "edit-form" = "/admin/aegir/operations/types/{aegir_operation_type}/edit",
 *     "delete-form" = "/admin/aegir/operations/types/{aegir_operation_type}/delete",
 *     "collection" = "/admin/aegir/operations/types"
 *   }
 * )
 */
class EntityType extends AbstractEntityType {

  // @TODO Move the task order methods into a trait, with associated interface.

  /**
   * Load task order from saved config.
   */
  protected function loadTaskOrder() {
    $config_name = $this->getTaskOrderConfigName();
    $task_order = \Drupal::configFactory()->get($config_name)->get($config_name);
    return $task_order ?: [];
  }

  /**
   * Permanently save task order.
   */
  protected function saveTaskOrder(array $task_order) {
    $config_name = $this->getTaskOrderConfigName();
    $config = \Drupal::configFactory()->getEditable($config_name);
    $config->set($config_name, $task_order);
    $config->save();
  }

  /**
   * Return the config name for this operation type's task order.
   */
  protected function getTaskOrderConfigName() {
    // @TODO This will need to be abstracted when moved to a trait.
    return 'aegir_operation.task_order.' . $this->id();
  }

  /**
   * Return the weighted order to execute referenced tasks.
   */
  public function getTaskOrder() {
    $task_order = $this->loadTaskOrder();
    asort($task_order);
    return $task_order;
  }

  /**
   * Save the weighted order to execute referenced tasks.
   */
  public function setTaskOrder(array $task_order) {
    asort($task_order);
    return $this->saveTaskOrder($task_order);
  }

  /**
   * Return the task types referenced by this operation type in weighted order.
   */
  public function getOrderedTaskTypes() {
    $task_order = $this->getTaskOrder();

    $tasks = $this->getTaskTypes();
    $ordered_tasks = [];
    foreach ($task_order as $name => $weight) {
      // If a task is removed from an operation, it'll still be saved in the
      // task order. Drop it here.
      if (!array_key_exists($name, $tasks)) continue;
      $ordered_tasks[$name] = $tasks[$name];
      unset($tasks[$name]);
    }
    // Append any remaining tasks (i.e., that don't have a weight yet).
    $ordered_tasks += $tasks;

    return $ordered_tasks;
  }

  /**
   * Return the task types referenced by this operation type.
   */
  public function getTaskTypes() {
    $entityManager = \Drupal::service('entity_field.manager');
    $entity_type = $this->getEntityType()->getBundleOf();
    $bundle = $this->id();
    $fields = $entityManager->getFieldDefinitions($entity_type, $bundle);
    $tasks = [];
    foreach ($fields as $name => $definition) {
      if ($definition->getSetting('target_type') != 'aegir_task') continue;
      $name = array_pop($definition->getSetting('handler_settings')['target_bundles']);
      $tasks[$name] = $definition;
    }
    return $tasks;
  }

}
