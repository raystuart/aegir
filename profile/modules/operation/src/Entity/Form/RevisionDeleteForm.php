<?php

namespace Drupal\aegir_operation\Entity\Form;

use Drupal\aegir_api\Entity\Form\AbstractRevisionDeleteForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Ægir operation revision.
 *
 * @ingroup aegir_operation
 */
class RevisionDeleteForm extends AbstractRevisionDeleteForm {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $entity_manager = $container->get('entity.field_manager');
    return new static(
      $entity_manager->getStorage('aegir_operation'),
      $container->get('database')
    );
  }

}
