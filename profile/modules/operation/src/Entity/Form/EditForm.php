<?php

namespace Drupal\aegir_operation\Entity\Form;

use Drupal\aegir_api\Entity\Form\EntityFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for editing Ægir operations.
 *
 * @ingroup aegir_operation
 */
class EditForm extends EntityFormBase {

}
