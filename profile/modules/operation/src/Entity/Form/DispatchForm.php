<?php

namespace Drupal\aegir_operation\Entity\Form;

use Drupal\aegir_operation\Operation;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Form controller for editing Ægir operations prior to dispatch.
 *
 * @ingroup aegir_operation
 */
class DispatchForm extends EditForm {

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions['dispatch'] = [
      '#type' => 'submit',
      '#value' => $this->t('Dispatch'),
      '#submit' => [
        ['Drupal\inline_entity_form\ElementSubmit', 'trigger'],
        '::submitForm',
        '::save',
      ],
      '#button_type' => 'primary',
      '#attributes' => [
        'class' => [
          'use-ajax-submit',
        ],
      ],
    ];

    $actions['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => Url::fromRoute('aegir_operation.modal.close'),
      '#attributes' => [
        'class' => [
          'button',
          'btn',
          'btn-default',
          'use-ajax',
          'cancel-modal',
        ],
      ],
    ];

    return $actions;
  }

  /**
   * {@inheritdoc}
   *
   * Override parent::save(), to skip confirmation message and redirect.
   */
  public function save(array $form, FormStateInterface $form_state) {
    // Call the base (grand-parent) save() method, to avoid an extra success message.
    ContentEntityForm::save($form, $form_state);

    $form_state->setRedirect('aegir_operation.modal.close');

    $operation = new Operation($this->entity);
    $operation->dispatch();
  }

}
