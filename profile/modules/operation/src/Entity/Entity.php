<?php

namespace Drupal\aegir_operation\Entity;

use Drupal\aegir_api\Entity\AbstractEntity;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines an Ægir operation entity.
 *
 * @ingroup aegir_operation
 *
 * @ContentEntityType(
 *   id = "aegir_operation",
 *   label = @Translation("Operation"),
 *   bundle_label = @Translation("Operation type"),
 *   handlers = {
 *     "storage" = "Drupal\aegir_api\Entity\StorageBase",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\aegir_api\Entity\ListBuilderBase",
 *     "views_data" = "Drupal\aegir_api\Entity\ViewsDataBase",
 *     "translation" = "Drupal\aegir_api\Entity\TranslationHandlerBase",
 *
 *     "form" = {
 *       "default" = "Drupal\aegir_operation\Entity\Form\EditForm",
 *       "dispatch" = "Drupal\aegir_operation\Entity\Form\DispatchForm",
 *       "add" = "Drupal\aegir_operation\Entity\Form\EditForm",
 *       "edit" = "Drupal\aegir_operation\Entity\Form\EditForm",
 *       "delete" = "Drupal\aegir_api\Entity\Form\DeleteFormBase",
 *     },
 *     "access" = "Drupal\aegir_api\Entity\AccessControlHandlerBase",
 *     "route_provider" = {
 *       "html" = "Drupal\aegir_api\Entity\HtmlRouteProviderBase",
 *     },
 *   },
 *   base_table = "aegir_operation",
 *   data_table = "aegir_operation_field_data",
 *   revision_table = "aegir_operation_revision",
 *   revision_data_table = "aegir_operation_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer aegir operation entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   links = {
 *     "canonical" = "/admin/aegir/operations/{aegir_operation}",
 *     "add-page" = "/admin/aegir/operations/add",
 *     "add-form" = "/admin/aegir/operation/add/{aegir_operation_type}",
 *     "edit-form" = "/admin/aegir/operations/{aegir_operation}/edit",
 *     "delete-form" = "/admin/aegir/operations/{aegir_operation}/delete",
 *     "version-history" = "/admin/aegir/operations/{aegir_operation}/revisions",
 *     "revision" = "/admin/aegir/operations/{aegir_operation}/revisions/{entity_revision}/view",
 *     "revision_revert" = "/admin/aegir/operations/{aegir_operation}/revisions/{entity_revision}/revert",
 *     "translation_revert" = "/admin/aegir/operations/{aegir_operation}/revisions/{entity_revision}/revert/{langcode}",
 *     "revision_delete" = "/admin/aegir/operations/{aegir_operation}/revisions/{entity_revision}/delete",
 *     "collection" = "/admin/aegir/operations",
 *   },
 *   bundle_entity_type = "aegir_operation_type",
 *   field_ui_base_route = "entity.aegir_operation_type.canonical"
 * )
 */
// @TODO implement an OperationInterface to more easily validate method params
// for methods that handle Operation entities.
class Entity extends AbstractEntity {

  // @TODO log fields and status should probably move to a trait.

  /**
   * The fields that represent task logs.
   */
  protected $taskLogFields = [
    'task_log_output',
    'task_log_sequence',
    'task_log_timestamp',
  ];

  /**
   * Empty task log fields in preparation of new task log output.
   */
  protected function resetTaskLogFields() {
    foreach ($this->taskLogFields as $field) {
      // These fields are all multi-value (i.e., arrays), so just reset to an
      // empty array.
      $this->$field = [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['task_log_output'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Task log'))
      ->setDescription(t('The output resulting from the last dispatch of this task.'))
      ->setRevisionable(TRUE)
      ->setCardinality(-1);

    $fields['task_log_sequence'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Task log sequence'))
      ->setDescription(t('The sequences of the output from the last dispatch of this task.'))
      ->setRevisionable(TRUE)
      ->setCardinality(-1);

    $fields['task_log_timestamp'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Task log timestamps'))
      ->setDescription(t('The timestamps of the output from the last dispatch of this task.'))
      ->setRevisionable(TRUE)
      ->setCardinality(-1);

    $fields['task_exitcode'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Task exitcode'))
      ->setDescription(t('The exit code from the last dispatch of this task.'))
      ->setRevisionable(TRUE)
      ->setCardinality(1);

    return $fields;
  }

  /**
   * Flag that the operation has been dispatched.
   *
   * @TODO Add to an operation interface.
   */
  public function setStatusDispatched() {
    $this->operation_status = 'dispatched';
    $this->createNewRevision(TRUE);
    $this->resetTaskLogFields();
    $this->save();
  }

  /**
   * Flag that an error occurred with the operation.
   *
   * @TODO Add to an operation interface.
   */
  public function setStatusError() {
    $this->setStatus('error');
  }

  /**
   * Update the operation status from the latest log output.
   *
   * @TODO Add to an operation interface.
   */
  public function updateStatus() {
    $this->setStatusFromLog();
  }

  /**
   * Get the current status of the operation.
   */
  public function getStatus() {
    return $this->operation_status->first()->getString();
  }

  /**
   * Parse the log and update the operation status accordingly.
   *
   * @TODO This should probably move to an Ansible adapter.
   */
  protected function setStatusFromLog() {
    $line = $this->getStatusLineFromLog();
    # @TODO: The Ansible output has changed. Do we need to take the other
    #        status types into account?
    #ok=5\ \ \ \[0m\ changed=0\ \ \ \ unreachable=0\ \ \ \ failed=0\ \ \ \ skipped=0\ \ \ \ rescued=0\ \ \ \ ignored=0
    # @TODO: We used to include a 'warning' status here. But we dropped that
    #        Ansible output plugin. We should either use it, or remove it from
    #        the theme, etc.
    foreach (['unreachable', 'failed', 'changed', 'ok'] as $status) { # Order is important!
      if ($this->parseStatusLine($status, $line)) return $this->setStatus($status);
    }
    return $this->setStatus('unknown');
  }

  /**
   * Extract the value of a given field from the provided log output line.
   *
   * @TODO This should probably move to an Ansible adapter.
   */
  protected function parseStatusLine($field, $line) {
    $regex = '/' . $field . '=(?<' . $field . '>\d+)/';
    preg_match($regex, $line, $matches);
    return $matches[$field];
  }

  /**
   * Return the line from the log output that contains a status summary.
   *
   * @TODO This should probably move to an Ansible adapter.
   */
  protected function getStatusLineFromLog() {
    foreach ($this->task_log_output as $line) {
      $text = $line->getString();
      if (strpos($text, 'ok=') === FALSE) continue;
      if (strpos($text, 'changed=') === FALSE) continue;
      if (strpos($text, 'unreachable=') === FALSE) continue;
      if (strpos($text, 'failed=') === FALSE) continue;
      return $text;
    }
    return $this->setStatus('unknown');
  }

  /**
   * Set the operation's status.
   */
  protected function setStatus($status) {
    $this->operation_status = $status;
    $this->createNewRevision(FALSE);
    $this->save();
  }

}
