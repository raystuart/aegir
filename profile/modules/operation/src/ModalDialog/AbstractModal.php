<?php

namespace Drupal\aegir_operation\ModalDialog;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\aegir_operation\Entity\Entity as OperationEntity;

/**
 * A base modal dialog class.
 */
abstract class AbstractModal {

  /**
   * The title of the modal dialog.
   */
  protected $title = NULL;

  /**
   * Options to pass to the ajax command.
   */
  protected $options = [
    'dialogClass' => 'modal-dialog-class',
    'width' => '80%',
  ];

  /**
   * The Operation entity that this modal is handling.
   */
  protected $entity = NULL;

  /**
   * Provide an AJAX response to open a modal dialog.
   */
  // @TODO Add OperationInterface here, once it exists.
  public function openModal(OperationEntity $operation) {
    $this->setEntity($operation);

    $title = $this->getTitle();
    $content = $this->getContent();
    $options = $this->getOptions();

    $response = new AjaxResponse();
    $response->setAttachments(['library' => ['core/drupal.dialog.ajax']]);
    $response->addCommand(new OpenModalDialogCommand($title, $content, $options));

    return $response;
  }

  /**
   * Close a modal dialog.
   */
  public function closeModal() {
    $command = new CloseModalDialogCommand();
    $response = new AjaxResponse();
    $response->addCommand($command);
    return $response;
  }

  /**
   * Return the content to be displayed in the modal dialog.
   */
  abstract protected function getContent();

  /**
   * Return the title of the modal dialog.
   */
  protected function getTitle() {
    return $this->title;
  }

  /**
   * Return the modal dialg options.
   */
  protected function getOptions() {
    return $this->options;
  }

  /**
   * Return the Operation entity this modal is handling.
   */
  protected function getEntity() {
    return $this->entity;
  }

  /**
   * Set the Operation entity this modal is handling.
   */
  // @TODO Add OperationInterface here, once it exists.
  protected function setEntity($entity) {
    $this->entity = $entity;
  }

}
