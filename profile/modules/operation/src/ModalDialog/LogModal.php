<?php

namespace Drupal\aegir_operation\ModalDialog;

use Drupal\views\Views;

/**
 * Modal dialog to present an operation log.
 */
class LogModal extends AbstractModal {

  /**
   * {@inheritdoc}
   */
  protected $title = 'Operation log';

  /**
   * {@inheritdoc}
   */
  protected function getContent() {
    // @TODO Make the view and display configurable in the plugin settings.
    $view = Views::getView('task_log');
    $view->setDisplay('task_log_modal');
    $view->setArguments([$this->getEntity()->uuid()]);
    $render = $view->render();
    return $render;
  }

}
