<?php

namespace Drupal\aegir_operation;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\aegir_api\Entity\EntityInterface;
use Drupal\aegir_api\Entity\BackReferenceFinder;

class Operation {

  use StringTranslationTrait;

  /**
   * Construct an Ægir Operation object.
   *
   * @todo Ensure only operation entities are passed in (via an OperationEntityInterface?)
   */
  public function __construct(EntityInterface $entity) {
    $this->entity = $entity;
    $this->queue = \Drupal::service('aegir_queue.task_queue');
    $this->log = \Drupal::logger($entity->getEntityTypeId());
  }

  /**
   * Dispatch the operation's tasks with the top-level entity's marshaled variables.
   */
  public function dispatch() {
    $referencing_entity = $this->entity->getReferencingEntity();

    $this->log->notice($this->t('Marshaling variables from :label', [
      ':label' => $referencing_entity->label(),
    ]));
    $variables = $referencing_entity->marshal();

    $this->log->notice($this->t('Dispatching tasks from :label.', [
      ':label' => $this->entity->label(),
    ]));

    $this->entity->setStatusDispatched();

    /* @todo Make the Celery task configurable. */
    // @TODO Add a global "debug" flag to simplify testing IPC components.
    #$task = $this->queue->addTask('dispatcherd.ansible_debug', [$config, $variables]);
    $task = $this->queue->addTask('dispatcherd.ansible', [$this->getConfig(), $variables]);
    if (!$task) {
      $this->log->error($this->t('Failed to dispatch operation: :label.', [
        ':label' => $this->entity->label(),
      ]));
      $this->entity->setStatusError();
    }

    /* @todo Return the task/taskId, so that we can reconstruct the AsyncResult to check on task progress/status? */
  }

  /**
   * Return a list of referenced task UUIDs keyed by field name.
   */
  protected function getTaskUuids() {
    // @TODO add helper method(s) to allow us to iterate over a simpler array.
    $uuids = [];
    foreach ($this->entity->getBundle()->getTaskTypes() as $name => $task_field) {
      $field_name = $task_field->getName();
      $values = $this->entity->get($field_name)->referencedEntities();
      $task = reset($values);
      $uuids[$name] = $task->uuid();
    }
    return $uuids;
  }

  /**
   * Return the operation configuration to pass to the backend.
   */
  protected function getConfig() {
    $referer = $this->entity->getReferencingEntity();
    return [
      /* @TODO Make this configurable? */
      /* retrieve these from tasks */
      'roles' => $this->entity->marshalRoles(),
      'roles_path' => '/app/backend/ansible/roles/',
      'uuid' => $this->entity->uuid(),
      'uuids' => [
        'operation' => $this->entity->uuid(),
        $referer->getEntityTypeId() => $referer->uuid(),
        'tasks' => $this->getTaskUuids(),
      ],
    ];
  }

}
