<?php

namespace Drupal\aegir_operation\Plugin\views\field;

use Drupal\views\ResultRow;

/**
 * Field handler to present a button to run an operation.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("run_operation")
 */
class RunOperationButton extends AbstractOperationButton {

  /**
   * {@inheritdoc}
   */
  protected $modalRoute = 'aegir_operation.modal.dispatch';

  /**
   * {@inheritdoc}
   */
  protected function getDefaultLabel() {
    return $this->t('Run');
  }

  /**
   * {@inheritdoc}
   */
  protected function getModalUrlClasses() {
    $classes = parent::getModalUrlClasses();
    $classes[] = 'operation-run-button';
    return $classes;
  }

  /**
   * {@inheritdoc}
   */
  protected function buttonIsEnabled(ResultRow $row) {
    return $this->getEntity($row)->getStatus() !== 'dispatched';
  }

}
