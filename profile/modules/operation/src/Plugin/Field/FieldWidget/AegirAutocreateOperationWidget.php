<?php

namespace Drupal\aegir_operation\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Simple widget that hides operation reference fields.
 *
 * @FieldWidget(
 *   id = "aegir_operation_autocreate",
 *   label = @Translation("Operation autocreate"),
 *   field_types = {
 *     "operation_reference"
 *   },
 *   multiple_values = false
 * )
 */
class AegirAutocreateOperationWidget extends EntityReferenceAutocompleteWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $form_element = parent::formElement($items, $delta, $element, $form, $form_state);
    // Disable the widget, since this field is handled by AutocreateTrait.
    // @TODO Is this the best way to handle this? Is it preferable to somehow
    // exclude this field from form handling altogether? For example, currently
    // form_alter hooks should work, which add some flexibility, but may
    // interfere with how the AutocreateTrait handles these references.
    $form_element['#access'] = FALSE;
    return $form_element;
  }

}
