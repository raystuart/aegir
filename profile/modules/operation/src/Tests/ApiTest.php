<?php

namespace Drupal\aegir_operation\Tests;

use Drupal\simpletest\WebTestBase;
use Drupal\aegir_operation\Entity\Entity as Operation;

/**
 * Test to exercise API functions not covered by other tests.
 *
 * @group aegir
 */
class ApiTest extends WebTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'aegir_test_operation',
  ];

  public $profile = 'aegir';

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * Disable schema validation for the base theme.
   *
   * @see: https://www.drupal.org/node/2860072
   */
  protected function getConfigSchemaExclusions() {
    return array_merge(parent::getConfigSchemaExclusions(), ['bootstrap.settings']);
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->user = $this->drupalCreateUser([], 'TestUser');
    $this->user->addRole('aegir_operation_manager');
    $this->user->save();
    $this->drupalLogin($this->user);
  }

  /**
   * Tests that the home page loads with a 200 response.
   */
  public function testApiMethods() {
    $settings = [
      // Use one of the bundles exported to the 'aegir_test_operation' module.
      'type' => 'test_operation_type_1',
      'name' => 'TEST_OPERATION_A',
      'created' => time(),
      'user_id' => $this->rootUser->id(),
      'status' => 1,
    ];
    $operation = Operation::create($settings);

    $type = $operation->getType();
    $this->assertEqual($type, $settings['type'], 'Get type (bundle) of Ægir operation.');

    $name = $operation->getName();
    $this->assertEqual($name, $settings['name'], 'Get name of Ægir operation.');

    $new_name = 'TEST_OPERATION_B';
    $operation->setName($new_name);
    $name = $operation->getName();
    $this->assertEqual($name, $new_name, 'Set name of Ægir operation.');

    $created = $operation->getCreatedTime();
    $this->assertEqual($created, $settings['created'], 'Get creation time of Ægir operation.');

    $new_created = time() - 10;
    $operation->setCreatedTime($new_created);
    $created = $operation->getCreatedTime();
    $this->assertEqual($created, $new_created, 'Set name of Ægir operation.');

    $owner_id = $operation->getOwnerId();
    $this->assertEqual($owner_id, $settings['user_id'], 'Get owner of Ægir operation.');

    $new_user = $this->drupalCreateUser([], 'TestUser2');
    $operation->setOwner($new_user);
    $owner_id = $operation->getOwnerId();
    $this->assertEqual($owner_id, $new_user->id(), 'Set owner of Ægir operation.');

    $published = $operation->isPublished();
    $this->assertEqual($published, $settings['status'], 'Get published status of Ægir operation.');

    $unpublished = FALSE;
    $operation->setPublished($unpublished);
    $published = $operation->isPublished();
    $this->assertEqual($published, $unpublished, 'Set published status of Ægir operation.');

    $operation->save();
    $this->drupalGet('admin/aegir/operations/' . $operation->id());
    $this->assertResponse(200);
    $this->assertText($new_name, 'User with "Operation manager" role can access un-published Ægir operations.');

  }

}
