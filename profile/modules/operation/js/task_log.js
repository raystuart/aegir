(function ($) {
  // @TODO Ensure this runs when a modal is opened/closed multiple times within
  // a single page-load.
  $(".view-task-log").ready(function(){

    // Add a form element to control scrolling and refreshing behaviour.
    if ( !$("#task_log_controls").length ) {
      $('.view-task-log').parent().prepend('<div id="task_log_controls"><form><input type="checkbox" name="follow_task_log" checked>Follow task log.</form></div>');
    }

    // Schedule a refresh for every few seconds.
    var refresh_log;
    function autorefresh_log() {
      // Refresh the view so long as we're still expecting new log output.
      if ( $(".operation-row.dispatched").length ) {
        refresh_log = setInterval(function() {
          // Stop scrolling to the top of the view.
          delete Drupal.AjaxCommands.prototype.viewsScrollTop;

          if ($('input[name=follow_task_log]').prop('checked')) {
            // Scroll to the bottom of the view
            $("#drupal-modal").scrollTop($(".view-task-log").height());
          }

          $('.view-task-log').trigger('RefreshView');

          // If we're not expecting any more log output, exit the loop.
          if ( !$(".operation-row.dispatched").length ) {
            clearTimeout(refresh_log);
          }
        }, 200);
      }
    }
    autorefresh_log();

  })

})(jQuery);

