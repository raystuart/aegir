# TODO: re-enable and fix operation manager permissions
@operations @types @api @disabled
Feature: Create and manage Aegir operation types
  In order to define operation types that can be combined into applications,
  as a operation manager,
  I want to be able to manage Aegir operation bundles.

  Background:
    Given I am logged in as a "Operation manager"
      And I am on "admin/aegir/operations/types"


  Scenario: Create operation types
     When I click "Add Ægir operation type"
      And I fill in "Label" with "TEST_OPERATION_TYPE_NEW"
      And I fill in "Machine-readable name" with "test_operation_type_new"
      And I press the "Save" button
     Then I should be on "admin/aegir/operations/types"
      And I should see the success message "Created the TEST_OPERATION_TYPE_NEW operation type."

  Scenario: Update operation types
     When I click "TEST_OPERATION_TYPE_NEW"
     Then I should see the heading "Edit operation type" in the "header" region
     When I fill in "Label" with "TEST_OPERATION_TYPE_CHANGED"
      And I press the "Save" button
     Then I should be on "admin/aegir/operations/types"
     Then I should see the success message "Saved the TEST_OPERATION_TYPE_CHANGED operation type."

  Scenario: Delete operation types
     Then I should see the link "TEST_OPERATION_TYPE_CHANGED"
     When I click "TEST_OPERATION_TYPE_CHANGED"
     Then I should see the heading "Edit operation type" in the "header" region
     When I click "Delete" in the "content" region
     Then I should see the heading "Are you sure you want to delete TEST_OPERATION_TYPE_CHANGED?" in the "header" region
     When I press the "Delete" button
     Then I should be on "admin/aegir/operations/types"
     Then I should see the success message "The operation type TEST_OPERATION_TYPE_CHANGED has been deleted."

