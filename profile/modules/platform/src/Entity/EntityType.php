<?php

namespace Drupal\aegir_platform\Entity;

use Drupal\aegir_api\Entity\AbstractEntityType;

/**
 * Defines an Ægir platform type entity.
 *
 * @ConfigEntityType(
 *   id = "aegir_platform_type",
 *   label = @Translation("Platform type"),
 *   handlers = {
 *     "list_builder" = "Drupal\aegir_api\Entity\EntityType\ListBuilderBase",
 *     "form" = {
 *       "add" = "Drupal\aegir_api\Entity\EntityType\Form\EntityTypeFormBase",
 *       "edit" = "Drupal\aegir_api\Entity\EntityType\Form\EntityTypeFormBase",
 *       "delete" = "Drupal\aegir_api\Entity\EntityType\Form\DeleteFormBase"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\aegir_api\Entity\EntityType\HtmlRouteProviderBase",
 *     },
 *   },
 *   config_prefix = "aegir_platform_type",
 *   admin_permission = "administer aegir platform types",
 *   bundle_of = "aegir_platform",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/aegir/platforms/types/{aegir_platform_type}",
 *     "add-form" = "/admin/aegir/platforms/types/add",
 *     "edit-form" = "/admin/aegir/platforms/types/{aegir_platform_type}/edit",
 *     "delete-form" = "/admin/aegir/platforms/types/{aegir_platform_type}/delete",
 *     "collection" = "/admin/aegir/platforms/types"
 *   }
 * )
 */
class EntityType extends AbstractEntityType {

}
