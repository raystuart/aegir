<?php

namespace Drupal\aegir_platform\Entity\Form;

use Drupal\aegir_api\Entity\Form\AbstractRevisionDeleteForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Ægir platform revision.
 *
 * @ingroup aegir_platform
 */
class RevisionDeleteForm extends AbstractRevisionDeleteForm {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $entity_manager = $container->get('entity.field_manager');
    return new static(
      $entity_manager->getStorage('aegir_platform'),
      $container->get('database')
    );
  }

}
