<?php

namespace Drupal\aegir_platform\Entity\Form;

use Drupal\aegir_api\Entity\Form\AbstractRevisionRevertTranslationForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for reverting a Ægir platform revision for a translation.
 *
 * @ingroup aegir_platform
 */
class RevisionRevertTranslationForm extends AbstractRevisionRevertTranslationForm {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.field_manager')->getStorage('aegir_platform'),
      $container->get('date.formatter'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aegir_platform_revision_revert_translation_confirm';
  }

}
