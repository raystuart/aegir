@examples @revisions @api @wip @disabled
Feature: Create and manage Aegir examples
  In order to define examples that can be combined into applications,
  as a example manager,
  I want to be able to manage Aegir example entities.

  Background:
    Given I run "drush -y en aegir_test_example"
      And I am logged in as a "Example manager"
      And I am on "admin/aegir/examples"

  Scenario: Create a example for later scenarios
    Given I click "Add Ægir example"
      And I click "TEST_EXAMPLE_TYPE_1" in the "content" region
      And I fill in "Name" with "TEST_EXAMPLE_B"
      And I press the "Save" button
     Then I should see the success message "Created the TEST_EXAMPLE_B example."
      And I should see the link "Revisions" in the "tabs" region

  Scenario: Update the example to create some revisions
    Given I should see the link "TEST_EXAMPLE_B" in the "content" region
      And I click "TEST_EXAMPLE_B"
     When I fill in "Name" with "TEST_EXAMPLE_B-changed"
      And I check the box "Create new revision"
      And for "Revision log message" I enter "First test log message."
      And I press the "Save" button
     Then I should see the success message "Saved the TEST_EXAMPLE_B-changed example."
     When I click "Edit" in the "tabs" region
      And I fill in "Name" with "TEST_EXAMPLE_B"
      And I check the box "Create new revision"
      And for "Revision log message" I enter "Second test log message."
      And I press the "Save" button
      And I click "Revisions"
     Then I should see the heading "Revisions for TEST_EXAMPLE_B" in the "header" region
      And I should see the text "First test log message."
      And I should see the text "Second test log message."

  Scenario: View a example revision
    Given I click "TEST_EXAMPLE_B"
      And I click "Revisions"
     When I click "view revision" in the "First test log message." row
     Then I should see "Revision of TEST_EXAMPLE_B-changed from" in the "header" region

  Scenario: Revert a example revision
    Given I click "TEST_EXAMPLE_B"
      And I click "Revisions"
     When I click "Revert" in the "First test log message." row
     Then I should see the text "Are you sure you want to revert to the revision from" in the "header" region
     When I press "Revert"
     #Then I should see the success message "The example TEST_EXAMPLE_B has been reverted to the revision from"


