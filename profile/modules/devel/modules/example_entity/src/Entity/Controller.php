<?php

namespace Drupal\aegir_example\Entity;

use Drupal\aegir_api\Entity\AbstractController;
use Drupal\aegir_api\Entity\EntityInterface;

/**
 * Class Controller.
 *
 * Returns responses for Ægir example routes.
 *
 * @package Drupal\aegir_example\Entity
 */
class Controller extends AbstractController {

  protected $aegirEntityType = 'aegir_example';

  protected $aegirEntityLabel = 'aegir example';

  /**
   * {@inheritdoc}
   */
  public function revisionOverview(EntityInterface $aegir_example) {
    /*
     * *N.B.* This method _must_ override in the parent entity, since it can
     * only accept parameters _named_ after their entity type.
     * @todo: Untangle entity routing to clean this up.
     * @see: \Drupal\aegir_api\Entity\AbstractController::revisionOverview().
     */
    return parent::revisionOverview($aegir_example);
  }

}
