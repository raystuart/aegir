#!/bin/bash

orig=example
orig_dir='modules/example_entity'

get_script_dir () {
  SOURCE="${BASH_SOURCE[0]}"
  while [ -h "$SOURCE" ]; do
    DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
    SOURCE="$( readlink "$SOURCE" )"
    [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
  done
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  echo "$DIR"
}

die() { echo "$@" 1>&2 ; exit 1; }


[ "$#" -eq 1 ] || die "This script requires 1 arguments, $# provided."

dest_dir=$1
[ ! -d "$dest_dir" ] || die "Destination path, '$dest_dir', already exists."

module="${dest_dir##*/}"

echo "This script will create a new Ægir entity module named '$module' at '$dest_dir'."
read -p "Proceed? " -n 1 -r
echo # move to a new line
[[ ! $REPLY =~ ^[Yy]$ ]] && die "Aborting."

src_dir="$( dirname $(get_script_dir) )/${orig_dir}"
mkdir -p "$( dirname $dest_dir )"
cp -r $src_dir $dest_dir
cd $dest_dir

# Capitalize first letters, to handle class names.
Orig="$(tr '[:lower:]' '[:upper:]' <<< ${orig:0:1})${orig:1}"
Module="$(tr '[:lower:]' '[:upper:]' <<< ${module:0:1})${module:1}"

# Capitalize fully, to handle test entities.
ORIG="$(tr '[:lower:]' '[:upper:]' <<< ${orig})"
MODULE="$(tr '[:lower:]' '[:upper:]' <<< ${module})"

# Rename files and directories recursively.
find . -depth -execdir rename "s/$orig/$module/g" {} +
find . -depth -execdir rename "s/$Orig/$Module/g" {} +
find . -depth -execdir rename "s/$ORIG/$MODULE/g" {} +

# Find/replace recursively within files.
find . -depth -type f -exec sed -i'' -e "s/$orig/$module/g" {} +
find . -depth -type f -exec sed -i'' -e "s/$Orig/$Module/g" {} +
find . -depth -type f -exec sed -i'' -e "s/$ORIG/$MODULE/g" {} +
