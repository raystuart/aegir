@queue @queue-config @api @disable-on-ci
Feature: Aegir task queue configuration
  In order to allow tasks to be securely added to the task queue,
  as an Aegir administrator,
  I need to be able to configure the task queue.

  Background:
    Given I am logged in as an "Ægir administrator"
      And I am on "admin/aegir/queue"

  Scenario: Confirm default task queue configuration.
     Then I should see the heading "Task queue configuration" in the "header" region
      And the "Host" field should contain "rabbitmq"
      And the "Login" field should contain "aegir"
      And the "Password" field should contain "53cr3t!"

  Scenario: Check invalid task queue configuration.
     When I fill in "Login" with "invalid"
      And I fill in "Password" with "invalid"
      And I press "Check connection settings"
     Then I should see the following error messages:
          | error messages                                                     |
          | Failed to connect to the rabbitmq task queue. Error message: Failed to establish a AMQP connection. Check credentials. |
          | The configuration options have NOT been saved.                     |
          | Verify connection settings.                                        |
     Then I should not see the following success messages:
          | success messages                           |
          | The task queue is properly configured.     |
          | The configuration options have been saved. |

  Scenario: Ensure invalid task queue configuration is not saved.
     When I fill in "Login" with "invalid"
      And I fill in "Password" with "invalid"
      And I press "Save configuration"
     Then I should see the following error messages:
          | error messages                                                     |
          | Failed to connect to the rabbitmq task queue. Error message: Failed to establish a AMQP connection. Check credentials. |
          | The configuration options have NOT been saved.                     |
          | Verify connection settings.                                        |
     Then I should not see the following success messages:
          | success messages                           |
          | The task queue is properly configured.     |
          | The configuration options have been saved. |

  Scenario: Check valid task queue configuration.
     When I fill in "Host" with "rabbitmq"
      And I fill in "Login" with "aegir"
      And I fill in "Password" with "53cr3t!"
      And I press "Check connection settings"
     Then I should not see the following error messages:
          | error messages                                                     |
          | Failed to connect to the rabbitmq task queue. Error message: Failed to establish a AMQP connection. Check credentials. |
          | The configuration options have NOT been saved.                     |
          | Verify connection settings.                                        |
      And I should see the following success messages:
          | success messages                           |
          | Connection to dispatcherd worker succeeded.|
          | The task queue is properly configured.     |
          | Test task execution took                   |
      And I should not see the following success messages:
          | success messages                           |
          | The configuration options have been saved. |

  Scenario: Check round-trip task queue configuration.
    When I press "Check task queue"
    Then I should see the following success messages:
         | success messages                                                    |
         | Connection to dispatcherd worker succeeded.                         |
         | Test task execution took                                            |
         | seconds for dispatcherd.                                            |
         | Connection to relayd worker succeeded.                              |
         | Test task execution took                                            |
         | seconds for relayd.                                                 |
         | The task queue is properly configured.                              |
         | The relayd task queue is functioning correctly. Round-trip took     |
         | The dispatcherd task queue is functioning correctly. Round-trip took |

  Scenario: Change task queue configuration.
     When I fill in "Host" with "rabbitmq.alt"
      And I press "Save configuration"
     Then I should not see the following error messages:
          | error messages                                                     |
          | Failed to connect to the task queue. See the log for more details. |
          | The configuration options have NOT been saved.                     |
          | Verify connection settings.                                        |
      And I should see the following success messages:
          | success messages                           |
          | The task queue is properly configured.     |
          | The configuration options have been saved. |
          | Test task execution took                   |
      And the "Host" field should contain "rabbitmq.alt"
      And the "Login" field should contain "aegir"
      And the "Password" field should contain "53cr3t!"

  Scenario: Reset task queue configuration.
     When I press "Reset to defaults"
     Then I should see the success message "The configuration options have been reset to default values."
      And the "Host" field should contain "rabbitmq"
      And the "Login" field should contain "aegir"
      And the "Password" field should contain "53cr3t!"

  @wip @disabled
  Scenario: Check error conditions when relayd is not running.
    When I turn off the "relayd" daemon
     And I turn on the "dispatcherd" daemon
     And I press "Check connection settings"
    Then I should see the error message "Task relayd.echo execution exceeded timeout of 5 seconds for relayd. Check /var/log/aegir/relayd.log on the relayd server for more details."
     And I should see the success message "Connection to dispatcherd worker succeeded."
    When I press "Check task queue"
    Then I should see the error message "Task relayd.echo execution exceeded timeout of 5 seconds for relayd. Check /var/log/aegir/relayd.log on the relayd server for more details."
     And I should see the success message "Connection to dispatcherd worker succeeded."

  @wip @disabled
  Scenario: Check error conditions when dispatcherd is not running.
    When I turn off the "dispatcherd" daemon
     And I turn on the "relayd" daemon
     And I press "Check connection settings"
    Then I should see the error message "Task dispatcherd.echo execution exceeded timeout of 5 seconds for dispatcherd. Check /var/log/aegir/dispatcherd.log on the dispatcherd server for more details."
     And I should not see the success message "Connection to relayd worker succeeded."
    When I press "Check task queue"
    Then I should see the error message "Task dispatcherd.echo execution exceeded timeout of 5 seconds for dispatcherd. Check /var/log/aegir/dispatcherd.log on the dispatcherd server for more details."
     And I should not see the success message "Connection to relayd worker succeeded."
