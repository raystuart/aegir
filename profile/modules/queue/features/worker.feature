@queue @queue-worker @api @disable-on-ci
Feature: Aegir task queue worker service
  In order to allow tasks to be run on servers,
  as an Aegir administrator,
  I need to be able to run the task queue runner service.

  @wip @disabled
  Scenario: The task queue worker service is running.
    Given I run "ps aux | grep [r]elayd"
     Then I should get:
      """
      /usr/bin/python3 /usr/local/bin/celery --app=relayd worker
      """
      # @TODO: fix this
#    And I run "supervisorctl status relayd"
#    Then I should get:
#    """
#    relayd STARTED
#    """

  Scenario: The task queue worker can accept and process tasks.
     When I run "drupal aegir:echo SomeTestString"
     Then I should get:
      """
      SomeTestString
      """
     When I run "drupal aegir:echo AnotherTestString"
     Then I should get:
      """
      AnotherTestString
      """
