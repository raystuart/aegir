<?php

namespace Drupal\aegir_queue\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Drupal\Core\State\StateInterface;
use Drupal\Console\Annotations\DrupalCommand;
use Drupal\Console\Core\Command\ContainerAwareCommand;
use Drupal\Console\Core\Style\DrupalStyle;

/**
 * Class AegirValidateQueueCommand.
 *
 * DrupalCommand (
 *     extension="aegir_queue",
 *     extensionType="module"
 * )
 */
class AegirValidateQueueCommand extends ContainerAwareCommand {

  /**
   * {@inheritdoc}
   */
  public function __construct(StateInterface $state) {
    parent::__construct();
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    # @TODO: this method seems to trigger an error like:
    # relayd.queue_valid[]: Command output: Warning: mkdir(): Permission denied in /var/www/html/vendor/drupal/console-core/src/Utils/ConfigurationManager.php on line 261
    # in /var/log/aegir/relayd.error.log on the relayd/web container. Fix this!
    $this
      ->setName('aegir:validate_queue')
      ->setDescription($this->trans('commands.aegir.validate_queue.description'))
      ->setHidden(true)
      ->addArgument(
        'worker',
        InputArgument::REQUIRED,
        $this->trans('commands.aegir.validate_queue.arguments.worker')
      );
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $io = new DrupalStyle($input, $output);
    $args = $input->getArguments();

    # Set flag in drupal State API to indicate the worker queue is valid.
    $state_var = sprintf('aegir.%s.queue_valid', $args['worker']);
    $this->state->set($state_var, TRUE);

    $io->info($this->trans('commands.aegir.validate_queue.messages.success'));
  }

}
