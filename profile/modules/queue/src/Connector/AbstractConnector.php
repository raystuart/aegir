<?php

namespace Drupal\aegir_queue\Connector;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\aegir_api\Logger\AegirLoggerTrait;

/**
 * Class AbstractConnector.
 *
 * @package Drupal\aegir_queue
 */
abstract class AbstractConnector implements ConnectorInterface {

  use AegirLoggerTrait, StringTranslationTrait;

  /**
   * A factory for configuration objects.
   *
   * @var ConfigFactory
   */
  protected $configFactory;

  /**
   * A keyed array of config values.
   *
   * @var array
   */
  protected $config;

  /**
   * Constructs an AbstractConnector.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
    $this->config = $this->loadConfig()->get();
  }

  /**
   * Load current configuration.
   */
  protected function loadConfig() {
    return $this->configFactory->getEditable($this->getConfigName());
  }

  /**
   * {@inheritdoc}
   */
  abstract public function getConfigName();

  /**
   * Extract configuration from a FormState.
   */
  protected function getConfigFromFormState(FormStateInterface $form_state) {
    $config = [];
    foreach ($this->getConfigKeys() as $key) {
      $config[$key] = $form_state->getValue($key);
    }
    return $config;
  }

  /**
   * Return the keys for this connector's configuration.
   */
  protected function getConfigKeys() {
    return array_keys($this->loadConfig()->get());
  }

  /**
   * Update the current task queue configuration.
   */
  protected function setConfig(array $config) {
    $this->config = $config;
  }

  /**
   * Update the current task queue configuration with an individual attribute.
   */
  public function setConfigAttribute(string $key, string $value) {
    $this->config[$key] = $value;
  }

  /**
   * Return configuration.
   */
  protected function getConfig() {
    return $this->config;
  }

  /**
   * {@inheritdoc}
   */
  public function saveConfigFromFormState(FormStateInterface $form_state) {
    $this->setConfigFromFormState($form_state);
    $this->saveConfig();
  }

  /**
   * {@inheritdoc}
   */
  public function setConfigFromFormState(FormStateInterface $form_state) {
    $config = $this->getConfigFromFormState($form_state);
    $this->setConfig($config);
  }

  /**
   * Save the task queue configuration.
   */
  protected function saveConfig() {
    foreach ($this->getConfig() as $key => $value) {
      $this->loadConfig()->set($key, $value);
    }
    $this->loadConfig()->save();
  }

}
