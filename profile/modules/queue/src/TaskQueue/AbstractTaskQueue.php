<?php

namespace Drupal\aegir_queue\TaskQueue;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\aegir_queue\Connector\ConnectorInterface;

/**
 * Class AbstractTaskQueue.
 *
 * @package Drupal\aegir_queue
 */
abstract class AbstractTaskQueue implements TaskQueueInterface {

  use StringTranslationTrait;

  /**
   * A connector to the task queue.
   *
   * @var Drupal\aegir_queue\Connector\ConnectorInterface
   */
  protected $connector;

  /**
   * Constructs a basic TaskQueue object.
   *
   * @param Drupal\aegir_queue\Connector\ConnectorInterface $connector
   *   The connector to the queue.
   */
  public function __construct(ConnectorInterface $connector) {
    $this->connector = $connector;
  }

  /**
   * Retrieve the form for the connector.
   */
  public function getConnectorForm() {
    return $this->connector->getForm();
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigNames() {
    return [
      $this->connector->getConfigName(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function checkConnectorSettings(FormStateInterface $form_state) {
    $this->connector->setConfigFromFormState($form_state);
    return $this->testConnection();
  }

  /**
   * Save the form values for the connector.
   */
  public function saveConnectorSettings(FormStateInterface $form_state) {
    $this->connector->saveConfigFromFormState($form_state);
  }

  /**
   * {@inheritdoc}
   */
  abstract public function addTask(string $type, array $args, string $queue);

  /**
   * Test that a connection to the task queue works.
   */
  abstract protected function testConnection();

}
