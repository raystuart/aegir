<?php

namespace Drupal\aegir_task\Entity\Form;

use Drupal\aegir_api\Entity\Form\AbstractRevisionRevertTranslationForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for reverting a Ægir task revision for a translation.
 *
 * @ingroup aegir_task
 */
class RevisionRevertTranslationForm extends AbstractRevisionRevertTranslationForm {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.field_manager')->getStorage('aegir_task'),
      $container->get('date.formatter'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aegir_task_revision_revert_translation_confirm';
  }

}
