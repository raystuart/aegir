<?php

namespace Drupal\aegir_task\Entity\Form;

use Drupal\aegir_api\Entity\Form\AbstractRevisionDeleteForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Ægir task revision.
 *
 * @ingroup aegir_task
 */
class RevisionDeleteForm extends AbstractRevisionDeleteForm {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $entity_manager = $container->get('entity.field_manager');
    return new static(
      $entity_manager->getStorage('aegir_task'),
      $container->get('database')
    );
  }

}
