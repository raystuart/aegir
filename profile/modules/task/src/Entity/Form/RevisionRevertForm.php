<?php

namespace Drupal\aegir_task\Entity\Form;

use Drupal\aegir_api\Entity\Form\AbstractRevisionRevertForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for reverting a Ægir Task revision.
 *
 * @ingroup aegir_task
 */
class RevisionRevertForm extends AbstractRevisionRevertForm {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.field_manager')->getStorage('aegir_task'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aegir_task_revision_revert_confirm';
  }

}
