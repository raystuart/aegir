<?php

namespace Drupal\aegir_task\Entity;

use Drupal\aegir_api\Entity\AbstractEntityType;

/**
 * Defines the Ægir task type entity.
 *
 * @ConfigEntityType(
 *   id = "aegir_task_type",
 *   label = @Translation("Task type"),
 *   handlers = {
 *     "list_builder" = "Drupal\aegir_api\Entity\EntityType\ListBuilderBase",
 *     "form" = {
 *       "add" = "Drupal\aegir_api\Entity\EntityType\Form\EntityTypeFormBase",
 *       "edit" = "Drupal\aegir_api\Entity\EntityType\Form\EntityTypeFormBase",
 *       "delete" = "Drupal\aegir_api\Entity\EntityType\Form\DeleteFormBase"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\aegir_api\Entity\EntityType\HtmlRouteProviderBase",
 *     },
 *   },
 *   config_prefix = "aegir_task_type",
 *   admin_permission = "administer aegir task types",
 *   bundle_of = "aegir_task",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/aegir/tasks/types/{aegir_task_type}",
 *     "add-form" = "/admin/aegir/tasks/types/add",
 *     "edit-form" = "/admin/aegir/tasks/types/{aegir_task_type}/edit",
 *     "delete-form" = "/admin/aegir/tasks/types/{aegir_task_type}/delete",
 *     "collection" = "/admin/aegir/tasks/types"
 *   }
 * )
 */
class EntityType extends AbstractEntityType {

}
