# TODO: re-enable and fix task manager permissions
@tasks @access @api @disabled
Feature: Access to Aegir tasks and types
  In order to define tasks that can be combined into applications,
  as a task manager,
  I need to be able to access Aegir task entities and types.

  Scenario: Anonymous users should not have access to tasks configuration.
    Given I am not logged in
     When I am on "admin/aegir"
     Then I should see "Access denied"

  Scenario: Authenticated users should not have access to tasks configuration.
    Given I am logged in as an "Authenticated user"
     When I am on "admin/aegir"
     Then I should not see the link "Tasks"

  Scenario: Task managers should have access to tasks configuration.
    Given I am logged in as a "Task manager"
     When I am on "admin/aegir"
     Then I should see the link "Tasks"
      And I should see the text "Ægir task entities and bundles"
     When I click "Tasks"
     Then I should be on "admin/aegir/tasks"
      And I should see the heading "Tasks" in the "header" region
      And I should see the link "Add Ægir task"
      And I should see the link "Task types"
     When I click "Task types"
     Then I should be on "admin/aegir/tasks/types"
      And I should see the heading "Task types" in the "header" region
      And I should see the link "Add Ægir task type"

