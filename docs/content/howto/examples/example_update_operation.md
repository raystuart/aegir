---
title: Example Update Operation

---

The Example Update Operation module builds on the existing [Example site](../example_site), adding a new operation to update a frontend field via backend task. It demonstrates how to update data in field (on a task) in the front-end from the back-end.

We define a second Example Site with Update (`example_site_update`), which reuses the `example_log_debug_data` Operation from `example_site`.

The Example Update Operation module defines a new Operation (`example_update_debug_data`) and associated Task (`example_write_debug_data`). The corresponding role (also named `example_write_debug_data`) uses the `aegir_input` Ansible module to send data to the `relayd` worker, which, in turn calls the `aegir:input` Drupal Console command, to write to the appropriate field.

We start with a simple Task type "Write debug data" (`example_write_debug_data`). This Task type contains a single text field, "New data" (`field_example_new_data`). The associated role (`example_write_debug_data`) will call the `aegir_input` role, passing in the data from the front-end field, as well as the UUID of the Task entity to update. Note that the role must be named the same as the task type. Also the role must be symlinked from its module directory to the `backend/ansible/roles` directory.

*N.B.* Under normal circustances, the data being written to the front-end using this mechanism would be generated on the backend (e.g., the path of a timestamped backup file.)

Next, we define a new Operation type, "Update debug data" (`example_update_debug_data`). This Operation type contains a single Task reference field, "Write debug data" (`field_example_write_debug_data`). Task reference fields define the type of Task that will be created when their Operation is created. In this case, when an "Update debug data" Operation is created, a "Write Debug Data" Task will be automatically created too, and referenced from the Task reference field.

Finally, we add our new Operation type to the existing Example Site (`example_site`) type. We create a new Operation reference field, "Update debug data" (`field_example_update_debug_data`). Just as with the Task reference field (above), an instance of the relevant Operation will be created when a site of this type is created.

With these pieces in place, you can now create an "Example site with update" site. You can then run the "Update debug data" Operation, which will prompt you to enter a value for the "Debug data" field. This will then dispatch the operation to the backend, which will write its output back to the operation's log. In the log, we can observe that the value we provided is indeed set, and available to Ansible.

TODO: describe the display configs (and possibly ones missing for the site type):
* `core.entity_form_display.aegir_site.example_site_update.default.yml`
* `core.entity_form_display.aegir_operation.example_update_debug_data.default.yml`
* `core.entity_form_display.aegir_task.example_write_debug_data.default.yml`
* `core.entity_view_display.aegir_site.example_site_update.default.yml`
* `core.entity_view_display.aegir_operation.example_update_debug_data.default.yml`
* `core.entity_view_display.aegir_task.example_write_debug_data.default.yml`
