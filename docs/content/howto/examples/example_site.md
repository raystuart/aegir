---
title: Example Site

---

The Example Site module is a minimalist implementation of an application in Aegir5. It demonstrates how data from a field (on a task) in the front-end can be used in the back-end.

The Example Site module defines a basic Aegir Site type (`example_site`), with a single Operation (`example_log_debug_data`) and associated Task (`example_print_debug_data`).

We start with a simple Task type "Print debug data" (`example_print_debug_data`). This Task type contains a single text field, "Debug data" (`field_example_debug_data`). The associated role (`example_print_debug_data`) will simply print all the variables available to Ansible, including this text field. Note that the role must be named the same as the task type. Also the role must be symlinked from its module directory to the `backend/ansible/roles` directory.

Next, we define a basic Operation type, "Log debug data" (`example_log_debug_data`). This Operation type contains a single Task reference field, "Print debug data" (`field_example_print_debug_data`). Task reference fields define the type of Task that will be created when their Operation is created. In this case, when a "Log debug data" Operation is created, a "Print Debug Data" Task will be automatically created too, and referenced from the Task reference field.

Finally, we define our Site type "Example site" (`example_site`). It contains a single Operation reference field, "Log debug data" (`field_example_log_debug_data`). Just as with the Task reference field (above), an instance of the relevant Operation will be created when a site of this type is created.

With these pieces in place, you can now create an Example site. You can then run the "Log debug data" Operation, which will prompt you to enter a value for the "Debug data" field. This will then dispatch the operation to the backend, which will write its output back to the operation's log. In the log, we can observe that the value we provided is indeed set, and available to Ansible.

TODO: describe the display configs (and possibly ones missing for the site type):
* `core.entity_form_display.aegir_operation.example_log_debug_data.default.yml`
* `core.entity_form_display.aegir_site.example_site.default.yml`
* `core.entity_form_display.aegir_task.example_print_debug_data.default.yml`
* `core.entity_view_display.aegir_operation.example_log_debug_data.default.yml`
* `core.entity_view_display.aegir_site.example_site.default.yml`
* `core.entity_view_display.aegir_task.example_print_debug_data.default.yml`
