---
title: Add an operation
weight: 20

---

We don't have any operations defined for Hugo yet. We'll need to add one before we can enable it on our new hugo site type. So, let's add an operation to deploy a Hugo site:

1. Go to `Ægir > Operations > Operation Types`.
1. Click "Add Ægir operation type".
1. Provide a label (e.g. "Hugo: Deploy site").

Next, we'll want to register this operation with the site type. So edit the "Hugo" site type, click the "Manage fields" tab, and then the "Add field" button.

In the "Add a new field" dropdown, select "Reference > Operation", and then provide a label (e.g., "Hugo Deploy Site"), and click "Save and continue". Then click "Save field settings".

Finally, select the Operation type that we had previously created, and click "Save settings".

