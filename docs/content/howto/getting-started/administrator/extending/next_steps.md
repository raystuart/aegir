---
title: Next steps
weight: 100

---

* Borrow from `drupal_create_vhost`, for the final task type.
* Add references to the tasks in the deploy operation type.
* Add a reference to the deploy operation in the Hugo site type.
* Export the Feature.
* Create a role for each task.
* Symlink the roles into `backend/ansible/roles`
* Test, debug, repeat.

