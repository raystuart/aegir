---
title: Extending Ægir

---

Ægir is designed to make it straight-forward to add support for hosting new applications. To illustrate this, let's add support for Hugo, a popular static site generator. Hugo builds stand-alone sites, and doesn't support multi-site scenarios. This simplifies things, as we won't need a separate platform type.

{{< children >}}

