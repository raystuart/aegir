---
title: Behaviour-Driven Development
menuTitle: BDD
weight: 20
---

We're already largely tooled up for [BDD](https://en.wikipedia.org/wiki/Behavior-driven_development), using [Drumkit](http://drumk.it) and [Behat](http://behat.org).

The workflow for adding a new feature involves:

1. Create new issue here in the main (distro) project (we can then dispatch it to a separate module, if req'd.)
1. Label it as a "Feature request".
1. Flesh out one or more user stories.
1. (Optional) Depending on scope and such, split out multiple stories into separate issues.
1. (Optional) Fork this repo, if the developer doesn't have commit access.
1. Create one or more new feature branches.
1. Begin writing tests that capture the components and workflows.
1. Run the new tests to ensure they fail.
1. Begin building Features to satisfy the tests.
1. Begin documenting the new feature, within the UI, here in the docs, and perhaps using the Tour module to introduce more complex workflows.
1. Once reasonably complete, with passing tests and good documentation, create a merge request.
1. Once we're satisfied with the state of the implementation and tests, merge the feature branch.
1. Update draft of release notes for next version.

To get started, launch a [development environment](../dev_environment) and [write some tests](../writing_tests).
