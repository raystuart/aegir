---
title: Platform/Site Sequence
weight: 10
---

This sequence represents the user (DevOps Engineer) interacting with the Aegir
frontend to initiate an Operation via a Platform or Site (Aegir Entity):

{{<mermaid>}}
sequenceDiagram
    participant U as User
    participant P as Platform/Site
    participant O as Operation
    participant T as Task
    participant Q as Queue
    activate U
    U->>P: Views platform page
    activate P
    loop Operations view
        P->>O: List operations
        activate O
        alt status == dispatched
            O-->>P: Disabled “Run” button
        else status != dispatched
            O-->>P: Enabled “Run” button
        end
        deactivate O
    end
    P-->>U: Display operations
    deactivate P
    alt Operation requires user input
        U->>O: Trigger ("run") operation
        activate O
        loop Inline forms
            O->>T: Build task form
            activate T
            T-->>O: Return task form
            deactivate T
        end
        O-->>U: Open operation form modal dialog
        deactivate O
        U->>O: Fills form & dispatches operation
        activate O
    else No user input required, dispatch directly
        U->>O: Trigger ("run") operation
    end
    deactivate U
    O->>P: Marshal platform variables
    activate P
    loop All platform operations
        P->>+O: Marshal operation variables
        loop All operation tasks
            O->>T: Marshal task variables
            activate T
            T-->>O: Return task variables
            deactivate T
        end
        O-->>-P: Return operation variables
    end
    P-->>O: Return platform variables
    deactivate P
    loop Operation tasks
        O->>T: Marshal task roles
        activate T
        T-->>O: Return task roles
        deactivate T
    end
    O->>Q: Dispatch to backend
    deactivate O
#
#    Note right of T: Rational thoughts <br/>prevail...
{{< /mermaid >}}

