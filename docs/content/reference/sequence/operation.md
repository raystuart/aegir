---
title: Operation Sequence
weight: 20
---

This sequence illustrates the flow from an Operation being triggered to run,
through placing a task on the queue, running the backend playbook, and getting
feedback returned to the frontend.

{{<mermaid>}}
sequenceDiagram
    participant P as Platform/Site
    participant O as Operation
    participant Q as Queue
    participant W as Worker
    participant A as Ansible
    activate Q
    activate W
    Q->>W: Receives operation
    Note over W: Write temporary playbook and vars
    W->>A: Execute temporary playbook
    activate A
    loop Every second
        W->>A: Poll for output
        A-->>W: Return latest output
        W->>O: Send output to front-end log
    end
    opt If needed
        A->>P: Updates values in front-end
    end
    A-->>W: Sends return code
    deactivate A
    W->>O: Sends return code
    activate O
    Note over O: Parses output to update operation status
    O->>P: Update operation status
    deactivate O
    deactivate W
    deactivate Q
{{< /mermaid >}}
