---
title: Queue System
weight: 20
---

## Queue system architecture

The queue is implemented using
[Celery](https://docs.celeryproject.org/en/stable/getting-started/introduction.html),
a full-featured task queue written in Python. The "backend" server runs
[RabbitMQ](https://www.rabbitmq.com/) and [Redis](https://redis.io/) to be our
Celery broker and result store, respectively.

The queue system thus consists of the RabbitMQ/Redis services on top of which
we have 2 Celery workers:

* [`relayd`](https://gitlab.com/aegir/aegir/-/blob/main/backend/relayd.py)
  runs on the front-end server, and serves primarily to relay data back into
  the front-end Drupal system about backend Operations and Tasks (status, log data, etc).
* [`dispatcherd`](https://gitlab.com/aegir/aegir/-/blob/main/backend/dispatcherd.py)
  runs on the back-end server, and serves primarily to dispatch Tasks to a
  backend plugin (currently Ansible, but eventually
  [anything](https://gitlab.com/aegir/aegir/-/issues/52)), to actually
  provision something (Platform, Site, Server, Service), or *do* something with
  the provisioned resources (run a backup, perform updates, etc).

## 2 Workers, 2 Queue exchanges

We'd originally assumed the Celery queue would be shared between these two
worker tasks, but that doesn't seem to be the right model.

Instead, we've determined that Celery uses the concept of different queues or
exchanges (what we're thinking of as "channels" on top of the underlying
"bus"), that use AMQP routing to get tasks to the correct worker. Workers in
turn specify which queue or queues they want to listen to when they start up,
and we specify which queue to put things on when we post a task.

Worker applications seem to need to be able to handle any task that gets
put on the queue they're listening on, and if a task comes in for a worker that
doesn't have a method to handle it, things fail badly.

As such, we've refactored the AbstractTaskQueue and related classes to take an
"exchange" argument, and similarly configured `dispatcherd` and `relayd` to
specify a particular queue/exchange/channel when they start up. This needs to
be better documented, and we probably need to understand the Celery, RabbitMQ,
and AMQP pieces here, or at least point our docs to the relevant docs for those
tools.

See commits
[d05e9bf](https://gitlab.com/aegir/aegir/-/commit/d05e9bf4d067ca68a35202a9c85323c8cf0942e5),
[1fee690](https://gitlab.com/aegir/aegir/-/commit/1fee6905e75101178ae8c272b49eb3adc6ef3153),
[2ed8c72](https://gitlab.com/aegir/aegir/-/commit/2ed8c72af1d73f352d58f137770adad6cfadfa0d)
for the related changes here.

## Queue mechanism components

@TODO: expand on these components and how they fit together.

* [ ] Docker Compose file(s); eg. [.ddev/docker-compose.dispatcherd.yml](https://gitlab.com/aegir/aegir/-/blob/68-implement-ansible-module-for-logging/.ddev/docker-compose.dispatcherd.yml)
* [ ] Docker image(s); eg.
    * [ ] [Packer build config](https://gitlab.com/aegir/aegir/-/blob/68-implement-ansible-module-for-logging/build/packer/docker/dispatcherd.json)
    * [ ] Provisioning scripts:
        * [ ] [Shell](https://gitlab.com/aegir/aegir/-/tree/68-implement-ansible-module-for-logging/build/packer/scripts)
        * [ ] Ansible:
            * [ ] [Ansible config file](https://gitlab.com/aegir/aegir/-/blob/68-implement-ansible-module-for-logging/build/ansible/ansible.cfg)
            * [ ] [Ansible inventory file](https://gitlab.com/aegir/aegir/-/blob/68-implement-ansible-module-for-logging/build/ansible/hosts)
            * [ ] [Playbook](https://gitlab.com/aegir/aegir/-/blob/68-implement-ansible-module-for-logging/build/ansible/dispatcherd.yml)
            * [ ] [Role](https://gitlab.com/aegir/aegir/-/tree/68-implement-ansible-module-for-logging/build/ansible/roles/aegir.workers)
* [ ] [DDEV command to re-deploy and re-start the queue worker](https://gitlab.com/aegir/aegir/-/blob/68-implement-ansible-module-for-logging/.ddev/commands/dispatcherd/dispatcherd)
* [ ] [Make targets for Packer image builds](https://gitlab.com/aegir/aegir/-/blob/68-implement-ansible-module-for-logging/drumkit/mk.d/20_ci.mk)
* [ ] [Make targets for DDEV](https://gitlab.com/aegir/aegir/-/blob/68-implement-ansible-module-for-logging/drumkit/mk.d/20_ddev.mk) (especially pulling new images)
* [ ] Incorporate these images into CI as services
* [ ] document the Drupal console commands `aegir:input`, `aegir:log`, and `aegir:exitcode` and how they interact with relayd/dispatcherd

## Queue Validation mechanisms

In [Issue #64](https://gitlab.com/aegir/aegir/-/issues/64), we implemented the
`drupal aegir:validate_queue` command (see commits
[ab3b2d92](https://gitlab.com/aegir/aegir/-/commit/ab3b2d92182a2a882099f7276fa13189e6d8e9b4),
[7d76e431](https://gitlab.com/aegir/aegir/-/commit/7d76e4310441be49f90580256c2f4704f6cc749c),
[f061785c](https://gitlab.com/aegir/aegir/-/commit/f061785c17251259bcabcb6c2e921d80d4ba3fa0))
along with some testing/debugging mechanisms currently living alongside the
"Check connection settings" page (`admin/aegir/queue`), called "Check task
queue". It works like this:

1. The [form validation code](https://gitlab.com/aegir/aegir/-/blob/64-implement-frontend-worker-daemon/profile/modules/queue/src/TaskQueue/CeleryTaskQueue.php#L27) simply confirms that the 2 Celery workers are up and running, by posting an "echo" task to each of them, and waiting for them to respond directly (see links to relayd.py and dispatcherd.py `@task.echo` functions)
    * note this is the same routine that runs when you click "Check connection settings", to first validate the workers are up
2. The [new TaskQueueConfigForm::checkQueueValidity() form submit handler](https://gitlab.com/aegir/aegir/-/blob/64-implement-frontend-worker-daemon/profile/modules/queue/src/Form/TaskQueueConfigForm.php#L150) actually posts a Celery task onto the queue, which we've called `queue_valid`. Here again, we've implemented both a [`relayd.queue_valid`](https://gitlab.com/aegir/aegir/-/blob/64-implement-frontend-worker-daemon/backend/relayd.py#L26) and [`dispatcherd.queue_valid`](https://gitlab.com/aegir/aegir/-/blob/64-implement-frontend-worker-daemon/backend/dispatcherd.py#L30)] routine for each of the workers, and they are triggered in turn. Both use the same technique: set a [State API](https://www.drupal.org/docs/8/api/state-api/overview) variable to `FALSE` initially, then call the worker task whose job is to end up setting that same variable to `TRUE`. After posting the task, the submit handler currently polls the State variable (resetting the cache each time through the wait loop), and returns TRUE when the State variable changes, or FALSE if it times out.
    * In the case of the `relayd.queue_valid` task, we simply call `drupal aegir:queue_valid` immediately, validating that we can have the Python Celery task code call out to a `drupal aegir` command, in turn feeding data back to the frontend Aegir site.
    * In the case of the `dispatcherd.queue_valid` task, we emulate the "round trip" feedback mechanism, where a backend task in turn posts a Celery task onto the queue for `relayd` to pick up and process (generally via a `drupal aegir` console command). In this case, we `dispatcherd.validate_queue` posts a `relayd.validate_queue`, which in turn calls the `drupal aegir:queue_valid` command, just as in the previous step.

