---
title: Architecture
weight: 1

---

## Summary

Fundamentally, Aegir5 maps a web GUI to CLI commands. It is made up of several components:

1. There is a **front-end** built on Drupal 8. This is the **default Admin UI** and **authoritative Object Model** for the sytem.
1. There is a **back-end** based on a RabbitMQ/Celery task queue to **dispatch, provision, and execute tasks**.
1. There are any number of **Minion servers**, on which **Applications and Services** can be provisioned.
1. The Drupal **REST API** enables any number of **alternate frontends** to manipulate the **Object Model** and trigger **Operations**.

The **front-end** UI allows users to construct the Object Model, creating Platforms and Sites to represent the applications and instances they need to provision, host, and manage.
Users can interact with Platforms and Sites by running Operations, composed of
Tasks, which pass configuration variables into a distributed task queue, built
on [Celery](https://docs.celeryproject.org).

The **back-end** Celery queue worker (`dispatcherd`) receives these tasks,
and runs provisioner commands, based on the variables passed into the task from the
front-end. Currently the only backend provisioner is Ansible, but the system is
designed to support pluggable provisioners such as Terraform, Kubernetes, or
plain shell scripts.

The back-end provisioners SSH into the Minion servers to manipulate the
Applications and Services running there. The minions may have a single
application stack or support multiple types of applications on a single server
(or container pod).

![Architecture diagram](/images/Aegir5-Architecture-Diagram.svg)

This diagram created with [yEd Live](https://www.yworks.com/yed-live/), [source GraphML](/images/Aegir5-Architecture-Diagram.graphml) file can be imported
there, or edited in the [yEd Desktop](https://www.yworks.com/products/yed) app.

**N.B.** This architecture reflect the original design proposal. While much of what has been built has stayed fairly close to this design, the above diagram reflects actual implementations more accurately.

![Original proposed architecture](/images/architecture_diagram.png)

{{< children >}}
