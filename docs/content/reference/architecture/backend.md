---
title: Backend
weight: 40
---

## Back-end

The back-end will consume tasks from the queue, and run various commands. The initial engine is `ansible-playbook`, but Kubernetes is also likely to follow.

### Queue worker

The back-end will be implemented as a Celery queue worker. This can then easily dispatch commands, as needed.

### Ansible roles

We can write small Ansible roles to represent each task we want to implement, rather than the usual model of handling all tasks related to a given application. A small set of variables will need to be mapped to the equivalent front-end fields.

For example, a task on the front-end to deploy a Drupal codebase, could trigger a `aegir.DeployDrupal8GitPlatform` role, where we provide variables for a git repo from whence to clone, as well as a filesystem path where it should be deployed.

A separate `aegir.VerifyDrupal8Platform` task could then ensure that proper file ownership and permissions are maintained. This should only require a path variable to be passed to the task. (note: we may not *need* verify anymore; this is just an example)

## Backend implementation (examples)

### Ansible-playbook queue worker

1. Celery queue worker receives task.
2. Writes temporary playbook (see example below) populated with vars and roles from the originating task.
3. Runs playbook.
4. Posts log output to front-end URL, where it populates the task log.
5. Roles may receive a "feedback_url" variable, whence it can post data required by the front-end (e.g., backup path, git commit hash, etc.)
    1. Role is responsible for a cURL (or whatever) call to post data to the provided URL.
6. Cleans up playbook.

### Temporary playbook example

   ```
     - hosts: web0
       vars:
          ( populated from task )
       roles:
          ( populated from task )
   ```


### Ansible role examples

"Task" roles are small re-usable pieces of config.

```
aegir.GenerateDrupalSiteBackup/
├── meta/main.yml (equiv. to .info)
├── defaults/main.yml (variable defaults)
└── tasks/main.yml (steps to execute)
    ├── drush archive-dump
    ├── move to storage location (path)
    └── post path back to front-end
```

```
aegir.WriteNginxVhost/
├── meta/main.yml
├── files/default-nginx-vhost.conf.j2
├── defaults/main.yml
└── tasks/main.yml
    ├── write vhost from template (possibly looking for overrides in various places)
    └── restart nginx (but only if the vhost changed; i.e. idempotence)
```

