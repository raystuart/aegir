---
title: Operations vs. Tasks
weight: 50
---

Operations expose "runnable" functionality to end-users. Operations group tasks into coherent sets (e.g., install CiviCRM site). Task, on the other hand, are atomic (e.g., write a vhost).

* Operation
    * SiteOperation
        * CmsSiteOperation
            * InstallCmsSiteOperation

                  ```
                  ::task_list = [
                      AnsibleWriteVhostTask,   <-- plug-in managers
                      AnsibleProvisionDatabaseTask,
                      AnsibleSiteInstallTask
                  ]
                  ```
            * BackupCmsSiteOperation
            * UpgradeCmsSiteOperation
        * CmsPlatformOperation
            * DeployCmsPlatformOperation
            * UpgradeCmsPlatformOperation
        * CmsDistributionOperation
            * UpdateCmsDistributionOperation
                * UpdateGitCmsDistributionOperation
                * UpdateComposerCmsDistributionOperation

_Plug-in Types_:

* Database:
    * MySql
    * Postgres
* Web:
    * Nginx
    * Apache
* Installer:
    * Drush
    * wp-cli
    * Drupal Console
    * cv

