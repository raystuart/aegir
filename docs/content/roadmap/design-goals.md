---
title: Design Goals
weight: 10
---

## Developer Experience (DX)

One of the overarching design goals of Aegir5 is to enable a simpler developer
experience than has been possible in Aegir3. We want to enable someone who
wants to deploy Wordpress on Aegir to do so with minimal knowledge of its
internals. One aspect of this is making clear boundaries in the form of APIs
and a protocol definition for how the different components can interact.

**[Which Developers?](../developers)**
